#include <iostream>

class Parent {
public:
    Parent() : parent_{} {}                                         // must be explicit
    Parent(const Parent& other) {
        parent_ = other.parent_;                                    // copy c'ctor doesn't assing child's data
    }
    virtual ~Parent() {}                                            // must be virtual
    void print() {
        std::cout << "> parent" << std::endl;
    }

    // table of pointers created (vtable)
    virtual void print_virtual() {
        std::cout << "> parent" << std::endl;
    }

private:
    int parent_;
};

class Child : public Parent {
public:
    void print() {
        std::cout << "> child" << std::endl;
    }
    
    void print_virtual() {
        std::cout << "> child" << std::endl;                        // pointer in vtable
    }
private:
    int child_;
};

int main() {
    Child childA;

    Parent& parentA = childA;                                       // reference utilizes polymorphism
    parentA.print();                                                // non-virtual method
    parentA.print_virtual();                                        // virtual method (uses polymorphism)

    Parent parentB{ Child() };                                      // copy initialization
                                                                    // object slicing (upcasting child to parent type)
    parentB.print_virtual();                                        // non-virtual (parent method called)

    return 0;
}
