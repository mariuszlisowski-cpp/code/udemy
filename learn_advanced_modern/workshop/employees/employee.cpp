#include "employee.hpp"

Employee::Employee(std::string name, std::string title, size_t seniority)
    : name_(name)
    , title_(title)
    , seniority_(seniority) {}

std::string Employee::getName() const {
    return name_;
}
std::string Employee::getTitle() const {
    return title_;
}
size_t Employee::getSeniority() const {
    return seniority_;
}

// to compile with ordered associative container (e.g. set, map or multi-)
bool Employee::operator<(const Employee& rhs) const {
    // sort by title if different
    if (this->title_ != rhs.title_) {
        return this->title_ < rhs.title_;
    }
    // if the same sort by seniority
    return this->seniority_ < rhs.seniority_;
}
