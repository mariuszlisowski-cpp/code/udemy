#include <iostream>
#include <vector>

class Test {
public:
    Test() {
        pBuffer_ = new int[SIZE]{};
    }
    Test(const Test& other) {                                       // copy c'tor
        memcpy(pBuffer_, other.pBuffer_,
               sizeof(*pBuffer_) * SIZE);
    }
    ~Test() {
        delete[] pBuffer_;
    }

private:
    static const int SIZE{ 100 };
    int* pBuffer_;
};

Test getTest() {
    return Test();                                                  // returning by value (temporary copy)
}

int main() {
    Test test;                                                      // lvalue (has an address)

    Test* pTestA = &test;                                           // address of lvalue
    // Test* pTestB = &getTest();                                   // error: taking address of rvalue

    int val{ 9 };
    int* pVal{ &++val };
    std::cout << *pVal << std::endl;
    delete pVal;

    // pVal = &val++;                                               // error: rvalue (as temporary before increment)
    // int* ptr{ &(7 + val) };                                      // error: rvalue in curly brackets

    return 0;
}
