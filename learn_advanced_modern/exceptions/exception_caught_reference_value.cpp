#include <exception>
#include <iostream>
#include <stdexcept>
#include <vector>

int main() {
    std::vector<int> vec(2);                                       // capacity

    try {
        auto value = vec.at(2);                                    // index too high
    } catch (const std::exception e) {                             // static binding as caught by value
        std::cerr << "> out of range: " << e.what() << std::endl;
    }
    
    // handling all subclasses of exception    
    try {
        auto value = vec.at(2); // index too high
    } catch (const std::exception& e) {                             // dynamic binding as caught by reference
        std::cerr << "> out of range: " << e.what() << std::endl;   // std::out_of_range caught (derived from exception)
    }

    // handling an appropriate exception
    try {
        auto value = vec.at(2); // index too high
    } catch (const std::out_of_range& e) {                          // dynamic binding as caught by reference
        std::cerr << "> out of range: " << e.what() << std::endl;
    }




    return 0;
}
