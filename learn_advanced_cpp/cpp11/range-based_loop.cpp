#include <iostream>

int main() {
    const char* nums[] {"one", "two", "three"};
    for (auto& num : nums) {
        std::cout << num << ' ';
    }
    std::cout << std::endl;

    
    auto numbers = {"one", "two", "three"}; // ERROR: auto nums {"one", "two", "three"} 
                                            // direct-list-initialization of 'auto' requires exactly one element
    for (auto& number : numbers) {
        std::cout << number << ' ';
    }
    std::cout << std::endl;

    std::string str{ "hello" };
    for (char c : str) {
        std::cout << c;
    }

    return 0;
}
