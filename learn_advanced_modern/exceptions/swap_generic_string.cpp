#include <iostream>
#include <string>

int main() {
    int a = 1, b = 9;

    // no-throw guarantee (no exceptions thrown)
    std::swap(a, b); // generic version
    std::cout << a << ", " << b << std::endl;

    std::string strA{"first"}, strB{"second"};

    // swapping the addresses only
    std::swap(strA, strB); // string version (from <string>)
    std::cout << strA << ", " << strB << std::endl;
    
    return 0;
}
