#include <iostream>
#include <memory>

int main() {
    std::unique_ptr<int> uptrA{std::make_unique<int>(99)};  // c++14
    *uptrA = 77;
    std::cout << *uptrA << std::endl;

    std::unique_ptr<float> uptrB(new float(1.6));           // c++11
    *uptrB = 3.14;
    std::cout << *uptrB << std::endl;

    return 0;
}
