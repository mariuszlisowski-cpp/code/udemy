#include <deque>
#include <iostream>

void movable(int&& y) {}

void function(int&& x) {
    x = 9;                      // argument is mutable
    movable(std::move(x));      // argument is moveable
}

int main() {
    

    return 0;
}
