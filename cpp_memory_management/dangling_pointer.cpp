/*  Rule of Three violated!
    If you have a destructor you also need:
    - copy constructor
    - copy assignment operator
*/
#include <iostream>

class Foo {
};

class Bar {
public:
    Bar() {
        foo_ = new Foo();
        std::cout << "> creating Bar at: " << this << std::endl;
    }
    ~Bar() {
        delete foo_;
        std::cout << "> deleting Bar at: " << this << std::endl;
    }

private:
    Foo* foo_;
};

int main() {
    Bar original;
    Bar copy{ original };                                               // shallow copy
                                                                        // points to the same addres
    return 0;
}                                                                       // Foo object destructed twice
