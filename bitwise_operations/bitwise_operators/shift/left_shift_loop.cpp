/* left shift - operator << */

#include <bitset>
#include <cstdint>
#include <iostream>

/*                integer_to_shift   <<   no_of_positions (unsigned)
        MSB is discarded per shift   <<   LSB is replaced by 0

                    1 0 0 0 0 0 0 1  <<   1 (one left)
    1 discarded  <- 0 0 0 0 0 0 1 0  <-   0 added
                    ^             ^
                  MSB             LSB
    most significant bit  |  least significant bit
*/

void print(uint8_t val) {
    std::cout << int(val) << '\t'
             << std::bitset<8>(val) << std::endl;
}

int main() {
    uint8_t value{0b00000001};
    print(value);

    for (auto i{1}; i < 8; ++i) {
        uint8_t result = value << i;
        print(result);
    }

    return 0;
}
