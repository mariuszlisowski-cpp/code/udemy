#include <chrono>
#include <iostream>
#include <string>
#include <thread>

int main() {
    std::thread th1([]{
        std::cout << std::this_thread::get_id() << std::endl;
    });
    std::thread th2([]{
        std::cout << std::this_thread::get_id() << std::endl;
    });

    th1.join();
    th2.join();

    return 0;
}
