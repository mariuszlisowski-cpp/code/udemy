#include <fstream>
#include <iostream>
#include <string>
#include <vector>

std::vector<std::string> file_read(std::string filename) {
    std::ifstream in_file(filename);
    std::vector<std::string> lines;
    std::string output;

    if (in_file) {
        // reads line till newline (LF)
        // returns false if nothing to read
        while (std::getline(in_file, output)) {
            lines.emplace_back(output + '\n'); // leaves CR (\r) (\x0d) if exists, but removes LF (\n) (\x0a)
        }
        in_file.close();
    } else {
        std::cerr << "No such file!" << std::endl;
    }

    return lines;
}

int main() {
    std::vector<std::string> lines {file_read("text")};

    for (const auto& line : lines) {
        std::cout << line;
    }

    return 0;
}
