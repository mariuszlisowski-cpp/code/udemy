#include <iostream>

class Base {
public:
    Base() : value_(1) {};
    Base(Base&& other) noexcept {
        std::swap(value_, other.value_);
        std::cout << "> base move constructor" << std::endl;
    }

// private:
    int value_;
};

class Derived : public Base {
public:
    Derived() : radius_(1.6) {};
    Derived(Derived&& other) noexcept : Base(std::move(other)) {
        std::swap(radius_, other.radius_);
        std::cout << "> derived move constructor" << std::endl;
    }

// private:
    float radius_;
};

int main() {
    Derived der_origin;
    std::cout << der_origin.radius_ << ", " << der_origin.value_ << std::endl;

    Derived der_moved{std::move(der_origin)};    // der_origin no longer exists
    std::cout << der_moved.radius_ << ", " << der_moved.value_ << std::endl;

    return 0;
}
