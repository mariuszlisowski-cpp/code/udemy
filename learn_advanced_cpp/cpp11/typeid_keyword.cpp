#include <iostream>
#include <string>
#include <typeinfo>

class Clazz {
};

int main() {
    std::cout << typeid(int).name() << std::endl
              << typeid(double).name() << std::endl
              << typeid(float).name() << std::endl
              << typeid(bool).name() << std::endl
              << typeid(std::string).name() << std::endl
              << typeid(Clazz).name() << std::endl;

    return 0;
}
