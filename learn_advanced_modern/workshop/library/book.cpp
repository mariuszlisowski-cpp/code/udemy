#include <iostream>

#include "book.hpp"

Book::Book(std::string autor, std::string title) 
        : autor_(autor), title_(title) {}

// must be defined if class used in a std::map (sorted)
bool Book::operator<(const Book& other) const {
    // are authors the same?
    if (this->autor_ != other.autor_) {
        std::cout << "> sort by author" << std::endl;
        return this->autor_ < other.autor_;
    }
    // same authors so sort by title
    std::cout << "> sort by title" << std::endl;
    return this->title_ < other.title_;
}

std::string Book::getAuthor() const {
    return autor_;
}
std::string Book::getTitle() const {
    return title_;
}
