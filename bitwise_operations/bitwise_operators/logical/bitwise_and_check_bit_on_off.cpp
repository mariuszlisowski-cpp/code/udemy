/* check if bit is ON or OFF - symbol & */

#include <cstdint>
#include <iostream>

/* index    7  6  5  4 3 2 1 0th
   power  2^7                2^0
   weight 128 64 32 16 8 4 2 1

   value  00101001
          AND
   mask   00001000    3rd bit
   result 00001000    != 0 thus ON
    
   value  00101001
          AND
   mask   10000000    8th bit
   result 00000000    == 0 thus OFF

   truth table:
   0 AND 0 -> 0
   0 AND 1 -> 0
   1 AND 0 -> 0
   1 AND 1 -> 1       both true -> 1
*/

bool isBitOn(uint8_t value_to_check) {
    return value_to_check != 0 ? true : false;          // if zero, bit is off
}

int main() {
    uint8_t value{0b00101001};                          // 3rd bit is on

    {
    /* check 3rd bit */
    uint8_t mask3rd{0b00001000};                        // 3rd bit mask (decimal 8)
    uint8_t result = value & mask3rd;                   // bitwise AND
    
    std::cout << "3rd " << (isBitOn(result) ?
                            "bit is on" :
                            "bit is off") << std::endl;
    }

    {
    /* check 8th bit */
    uint8_t mask8th{0b10000000};                        // 8th bit mask (decimal 128)
    uint8_t result = value & mask8th;

    std::cout << "8th " << (isBitOn(result) ?
                            "bit is on" :
                            "bit is off") << std::endl;
    }

    return 0;
}
