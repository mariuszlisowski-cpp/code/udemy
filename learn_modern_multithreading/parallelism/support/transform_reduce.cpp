#include <execution>
#include <iostream>
#include <numeric>
#include <vector>

// using namespace std::execution;

int main() {
	std::vector<int> x{1, 2, 3, 4, 5};
	std::vector<int> y{5, 4, 3, 2, 1};

    // support execution policies (not in clang yet)
	auto result = std::transform_reduce(x.begin(), x.end(),    	// replaces std::inner_product
                                        y.begin(),
                                        0);
	
	std::cout << "First vector:  ";
	for (auto i : x)
		std::cout << i << ", ";
	
	std::cout << std::endl << "Second vector: ";
	for (auto i : y)
		std::cout << i << ", ";
	std::cout << std::endl;
	
	std::cout << "Result is " << result << std::endl;
}
