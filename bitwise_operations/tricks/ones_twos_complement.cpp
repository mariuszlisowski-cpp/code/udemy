#include <bitset>
#include <iostream>

int onesComplement(int value) {
    auto all_zeros = value & 0;
    auto all_ones = ~all_zeros;
    
    return value ^= all_ones;
}

int twosComplement(int value) {
    return onesComplement(value) + 1;                                                       // correct negative
}


int main() {
    auto value = 42u;

    std::cout << value << '\t' <<std::bitset<32>(value) << std::endl;

    auto ones = onesComplement(value);
    std::cout << ones << '\t' << std::bitset<32>() << std::endl;
    
    auto twos = twosComplement(value);
    std::cout << twos << '\t' << std::bitset<32>(twosComplement(value)) << std::endl;

    return 0;
}
