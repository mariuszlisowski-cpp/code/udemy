#include <exception>
#include <functional>
#include <future>
#include <iostream>
#include <stdexcept>
#include <thread>

using namespace std::literals;

void producer(std::promise<int>& p_value) {
    try {
        std::cout << "> setting a value..." << std::endl;
        std::this_thread::sleep_for(1s);
        throw std::out_of_range("bad value");
        p_value.set_value(9);                               // shared value not set
    } catch (...) {
        p_value.set_exception(std::current_exception());    // exception saved in promise
    }
}

void consumer(std::future<int>& f_value) {
    try {
        int x{f_value.get()};                               // wait until value is updated by promise
                                                            // can throw an excepion set in promise
        std::cout << "> value consumed" << std::endl;
    } catch (std::exception& e) {
        std::cout << "> exception caught: " << e.what() << std::endl;
    }
}

int main() {
     std::promise<int> promise;
     std::future<int> future{promise.get_future()};         // associated future

    std::thread consume(consumer, std::ref(future));
    std::thread produce(producer, std::ref(promise));

    consume.join();
    produce.join();

    return 0;
}
