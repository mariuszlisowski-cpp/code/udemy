#include <iostream>
#include <iterator>
#include <set>

void check_insertion_status(const std::pair<std::set<int>::iterator, bool>& res) {
	if (res.second) {
		std::cout << "> inserted successfully: " << *res.first << std::endl;
	} else {
		std::cout << "> duplicated value rejected: " << *res.first << " exists!" << std::endl;
	}
}

template<typename T>
void print_set(const std::set<T>& set) {
    std::copy(set.begin(), set.end(),
              std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
	// unique elements strored in order (using < operator)
    std::set<int> set; // implemented as a tree
	std::pair<std::set<int>::iterator, bool> result;
	
	result = set.insert(1); // const element
	check_insertion_status(result);
	
	result = set.insert(1);
	check_insertion_status(result);

    set.insert({5, 4, 3, 2, 1}); // will be sorted
    print_set(set);

    auto res = set.find(5);
    std::cout << (res != set.end() ? "found" : "not found") << std::endl;

    auto count = set.count(6); // can be 1 (found unique key) or 0 (no such element)
    std::cout << "> count: " << count << std::endl;

    set.erase(5);
    print_set(set);

	return 0;
}
