class X {};

/* forwarding reference (universal)
   argument deducted as one of:
   (T& x)       mutable     (lvalue)
   (const T& x) immutable   (lvalue)
   (T&& x)      moveable    (rvalue)
*/
template <typename T>
void function(T&& arg) {
    // if arg&&       - rvalue deducted
    // if arg&        - lvalue deducted (mutable)
    // if const arg&  - lvalue deducted (immutable)
}

int main() {
    function(X());      // xvalue

    X x;
    function(x);        // lvalue (modifable)

    const X xx;
    function(xx);       // lvalue (unmodifable)

    return 0;
}
