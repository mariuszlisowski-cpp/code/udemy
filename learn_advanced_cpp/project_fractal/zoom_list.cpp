#include "zoom_list.hpp"

namespace raisin {

zoom_list::zoom_list(int width, int height) : width_(width), height_(height) {}

void zoom_list::add(const zoom& z) {
    zooms_.push_back(z);

    x_center_ += (z.x_ - width_ / 2) * scale_;
    y_center_ += -(z.y_ - height_ / 2) * scale_;

    scale_ *= z.scale_;
}

std::pair<double, double> zoom_list::do_zoom(int x, int y) {
    double x_fractal{ (x - width_ / 2) * scale_ + x_center_ };
    double y_fractal{ (y - height_ / 2) * scale_ + y_center_ };

    return std::pair<double, double>(x_fractal, y_fractal);
}

}
