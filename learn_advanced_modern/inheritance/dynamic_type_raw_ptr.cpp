#include <iostream>
#include <memory>
#include <vector>

class Drawable {
public:
    virtual ~Drawable() {}
    virtual void draw() = 0;
};

class Circle : public Drawable {
public:
    // also virtual
    void draw() override {
        std::cout << "> circle drew" << std::endl;
    }
};

class Triangle : public Drawable {
public:
    void draw() override {
        std::cout << "> triangle drew" << std::endl;
    }
};

class Rectngle : public Drawable {
public:
    void draw() override {
        std::cout << "> rectangle drew" << std::endl;
    }
};

int main() {
    std::vector<Drawable*> shapes;

    // dynamic type (dynamic binding when a decision is made at a runtime)
    shapes.push_back(new Circle());
    shapes.push_back(new Triangle());
    shapes.push_back(new Rectngle());

    // pointer can be passed by value
    for (const auto shape : shapes) {
        shape->draw();
        delete shape;   // memory deallocated
    }

    shapes.clear();     // nullptrs thus collection of no use

    return 0;
}
