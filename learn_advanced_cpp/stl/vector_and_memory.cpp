#include <iostream>
#include <vector>

int main() {
    std::vector<double> numbers(20);
    std::cout << "> size: " << numbers.size() << std::endl;

    auto capacity = numbers.capacity();
    std::cout << "> capacity: " << numbers.size() << std::endl;

    for (size_t i{ 0 }; i < 1000; ++i) {
        if (numbers.capacity() != capacity) {
            capacity = numbers.capacity();
            std::cout << "> capacity: " << capacity << std::endl;
        }
        numbers.push_back(i);
    }
    
    numbers.clear();                                                    // clearing vector
    std::cout << "> size: " << numbers.size() << std::endl;             // zero size
    std::cout << "> capacity: " << numbers.capacity() << std::endl;     // but still same capacity (memory waste)

    numbers.resize(99);
    std::cout << "> size: " << numbers.size() << std::endl;
    std::cout << "> capacity: " << numbers.capacity() << std::endl;     // still same capacity

    numbers.reserve(999);                                               // no effect (as capacity greater)
    std::cout << "> size: " << numbers.size() << std::endl;
    std::cout << "> capacity: " << numbers.capacity() << std::endl;     // still same capacity

    numbers.reserve(9999);
    std::cout << "> size: " << numbers.size() << std::endl;
    std::cout << "> capacity: " << numbers.capacity() << std::endl;     // new capacity (as reserved)

    numbers.clear();
    numbers.shrink_to_fit();
    std::cout << "> size: " << numbers.size() << std::endl;             // zero size
    std::cout << "> capacity: " << numbers.capacity() << std::endl;     // zero capacity

    return 0;
}
