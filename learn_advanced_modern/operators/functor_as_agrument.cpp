#include <iostream>
#include <vector>

// functor (class with an overloaded function call operator)
class Even {
public:
    // any number and type of arguments, return any type
    bool operator()(int value) const {  // may be const (must be if passed as const&)
        return !(value % 2);
    }
};

template<typename T, typename K>
void only_even_values(const std::vector<T>& vec, const K& boolean_functor) {
    for (const auto& el : vec) {
        // invoking operator() 
        if (boolean_functor(el)) {
            std::cout << el << ' ';
        }
    }
}

int main() {
    std::vector<size_t> positives{1, 2, 3, 4, 5, 6, 7, 8, 9};
    Even even;

    // passing functor as an argument
    only_even_values(positives, even);    

    return 0;
}
