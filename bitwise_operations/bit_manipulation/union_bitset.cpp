#include <iostream>
#include <iomanip>
#include <cstdint>

using Color = uint16_t;
struct RGB565 {
    Color red : 5;
    Color green : 6;
    Color blue : 5;
};

union RGB {
    RGB() {}
    RGB565 component;
    Color color;
};

int main() {
    RGB rgb;
    rgb.color = 0xffff;
    std::cout << std::hex << rgb.component.red << std::endl;
    std::cout << std::hex << rgb.component.green << std::endl;
    std::cout << std::hex << rgb.component.blue << std::endl;

    rgb.component.red = 0xa;
    rgb.component.green = 0xa;
    rgb.component.blue = 0xb;

    std::cout << std::hex << rgb.component.red << std::endl;
    std::cout << std::hex << rgb.component.green << std::endl;
    std::cout << std::hex << rgb.component.blue << std::endl;

    std::cout << std::hex << rgb.color << std::endl;

    return 0;
}
