#include <chrono>
#include <iostream>
#include <string>
#include <thread>

// no argument copying
void function(std::thread th) {
    std::cout << std::this_thread::get_id() << std::endl;
}

int main() {
    function(std::thread());    // temporary thread (thus rvalue)

    std::thread th;             // lvalue
    function(std::move(th));    // cast to rvalue

    return 0;
}
