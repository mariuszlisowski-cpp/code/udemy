#include <numeric>
#include <iostream>
#include <vector>

int main() {
	std::vector<int> x{1, 2, 3, 4, 5};
	std::vector<int> y{5, 4, 3, 2, 1};

	// no support for execution policies
	auto result = std::inner_product(x.begin(), x.end(),	// supportive eqivalent is std::transform_reduce
                                     y.begin(),
                                     0);

	std::cout << "First vector:  ";
	for (auto i : x)
		std::cout << i << ", ";
	
	std::cout << std::endl << "Second vector: ";
	for (auto i : y)
		std::cout << i << ", ";
	std::cout << std::endl;
	
	std::cout << "Result is " << result << std::endl;            // Displays 35
}
