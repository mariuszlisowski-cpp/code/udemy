#include <iostream>
#include <string>

int main() {
    std::string str{"test"};
    std::string pi{std::to_string(3.14159)};

    str.insert(str.length(), 5, '.');   // index, count, char
    str.insert(0, 3, '_');              // index, count, char
    str.append("of string");
    str.append("!?", 0, 1);             // str, index, count
    str.append("\n").append(pi);        // chain

    std::cout << str << std::endl;
    std::cout << std::stod(pi) << std::endl;
    
    return 0;
}
