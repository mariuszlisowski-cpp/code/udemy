/* switch ON a single bit - inclusive OR - symbol | */

#include <bitset>
#include <cstdint>
#include <iostream>

/* index    7  6  5  4 3 2 1 0th
   power  2^7                2^0
   weight 128 64 32 16 8 4 2 1

   value  00000101
          OR
   mask   00000010    1st bit
   result 00000111    1st is ON
    
   truth table:
   0 OR 0 -> 0        both false -> 0
   0 OR 1 -> 1
   1 OR 0 -> 1
   1 OR 1 -> 1
*/

int main() {
    uint8_t value{0b00000101};                  // switch ON 1st bit (zero-indexed)
    uint8_t mask_{0b00000010};                  // mask of 1st bit

    uint8_t result = value | mask_;             // first operand bit NOT changed in 0 in mask

    std::cout << std::bitset<8>(result)
              << std::endl;
     
    return 0;
}
