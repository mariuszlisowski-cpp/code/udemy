#include <iostream>
#include <vector>

struct TestA {
    int i_;
    float f_;
};

struct TestB {
    double d_;
    size_t size_;
} testB;                                    // variable declaration

struct {                                    // no need for a type name (anonymous)
    double d_;
    size_t size_;
} testC = {3.14, 7}, testD = {3.2, 1};      // variable initialization (cpp98)

struct {
    std::string str_{"Hi"};                 // initialization (cpp11)
    long value_;
} testE;                                    // declaration (partly initialized)

int main() {
    /* cpp98 initialization */
    int values[] = {1, 2, 3};
    // struct
    TestA testA = { 3, 3.13 };
    testB.size_ = 7;
    testB.d_ = 3.14;

    /* cpp11 initialization */
    int value{ 7 };                         // no assignment operator needed
    int nums[]{ 1, 2, 3 };
    float* ptr{ new float[2]{6, 7} };       // number of elements compulsory
    std::cout << *(ptr + 1) << std::endl;

    double zero{};                          // default value (zero)
    int* pointer{};                         // default value (nullptr)
    // struct
    testE.value_ = 99;                      // initialization
    std::cout << testE.value_ << ' ';
    std::cout << testE.str_ << std::endl;   // already initialized

    std::vector<std::string> strs{
        "one", "two", "three", "four"
    };                                      // initializer list

    return 0;
}
