#include <iostream>
#include <memory>
#include <vector>

class Drawable {
public:
    virtual ~Drawable() {}      // destructor must be virtual
    virtual void draw() = 0;    // pure virtual
};

class Circle : public Drawable {
public:
    // virtual keyword is redundat
    virtual void draw() override {
        std::cout << "> circle drew" << std::endl;
    }
};

class Triangle : public Drawable {
public:
    // override keyword checks if acually overriding a base class method
    virtual void draw() override {
        std::cout << "> triangle drew" << std::endl;
    }
};

class Rectangle : public Drawable {
public:
    virtual void draw() override {
        std::cout << "> rectangle drew" << std::endl;
    }
};

int main() {
    std::vector<std::unique_ptr<Drawable>> shapes;

    // dynamic type (dynamic binding when a decision is made at a runtime)
    shapes.push_back(std::make_unique<Circle>());       // dynamic type to Circle (static to Drawable)
    shapes.push_back(std::make_unique<Triangle>());     // dynamic type to Triangle (static to Drawable)
    shapes.push_back(std::make_unique<Rectangle>());    // dynamic type to Rectangle (static to Drawable)

    // pointer must be a reference as no copy constructor for unique pointer
    for (const auto& shape : shapes) {
        shape->draw();
    }

    return 0;
}
