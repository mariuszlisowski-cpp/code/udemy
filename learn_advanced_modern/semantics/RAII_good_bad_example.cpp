// RAII resource acquisition is initialization
// ~ class manages its resources which are allocated in constructor and released in destructor

#include <iostream>

std::mutex m;

void f() {}

void bad() {
    m.lock();                          // acquire the mutex
    f();                               // if f() throws an exception                  - the mutex is never released
    if(true) return;                   // early return                                - the mutex is never released
    m.unlock();                        // if bad() reaches this statement             - the mutex is released
}
 
void good() {
    std::lock_guard<std::mutex> lk(m); // RAII: mutex acquisition is initialization
    f();                               // if f() throws an exception                  - the mutex is released
    if (true) return;                  // early return                                - the mutex is released
}

int main() {
    

    return 0;
}
