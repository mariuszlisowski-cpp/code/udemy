#include <algorithm>
#include <iomanip>      // std::quote()
#include <iostream>
#include <string>

int main() {
    std::string str{"this_is_a_quoted_sentence"};
    std::cout << "> before: " << std::quoted(str) << std::endl;
    
    std::cout <<  "> after:   ";
    std::replace_copy(str.begin(), str.end(),
                     std::ostream_iterator<char>(std::cout),
                     '_', ' ');


    return 0;
}
