#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template <class T>
void print_vector(std::vector<T> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> in{7, 1, 2, 3, 9};
    std::vector<int> out;

    int new_value = 0;
    auto predicate = [](int n){ return n > 3;};
    // replacement in a copy
    std::replace_copy_if(in.begin(), in.end(), std::back_inserter(out), predicate, new_value);

    print_vector<int>(in);
    print_vector<int>(out);

    return 0;
}
