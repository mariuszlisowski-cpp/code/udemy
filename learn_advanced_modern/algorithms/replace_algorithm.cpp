#include <algorithm>
#include <array>
#include <iostream>
#include <iterator>

template <class T>
void print_array(std::array<T, 5> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, " "));
}

int main() {
    std::array<int, 5> arr{9, 1, 2, 3, 9};

    int old_value = 9;
    int new_value = 0;
    // in-place replacement
    std::replace(arr.begin(), arr.end(), old_value, new_value);

    print_array<int>(arr);

    return 0;
}
