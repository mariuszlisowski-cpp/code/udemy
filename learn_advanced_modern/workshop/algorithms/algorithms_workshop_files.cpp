#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <random>
#include <set>
#include <string>
#include <vector>

template<typename T>
void print_vector(std::vector<T> vec) {
    for (auto el : vec) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::set<std::string> words;
    std::vector<std::string> words_all;
    std::vector<std::string> lines;
    
    std::string filename{"textA"};
    std::ifstream file_in(filename);
    if (file_in) {
        std::istream_iterator<std::string> is_it(file_in);
        std::istream_iterator<std::string> eof;
        
        words = {is_it, eof};                       // each word once

        if (file_in.eof()) {
            file_in.clear();                        // clear eof bit and
            file_in.seekg(0, std::ios::beg);        // return to the begining
            is_it = file_in;                        // reset iterator
        }        
        words_all = {is_it, eof};                   // all words

        if (file_in.eof()) {
            file_in.clear();                        // clear eof bit and
            file_in.seekg(0, std::ios::beg);        // return to the begining
        }        
        std::string line;
        while (std::getline(file_in, line)) {
            lines.push_back(line);                  // lines count
        }
        
        file_in.close();
    } else {
        std::cout << "> no such file '" << filename << "'" << std::endl;
    }

    // read a file of words and display each word once
    for (const auto& word : words) {
        std::cout << word << ' ';
    }
    std::cout << std::endl;

    // count the total number of words in the file
    std::cout << "> all words in a file: " << words_all.size() << std::endl;
    print_vector(words_all);

    // count the number of lines in the file
    std::cout << "> no of lines in a file: " << lines.size() << std::endl;
    
    // count the number of characters in the file
    int chars_count = 
        std::accumulate(lines.begin(), lines.end(), 0,
                        [](int sum, std::string& str) {
                            return sum + str.size();
                        });
    std::cout << "> characters count: " << chars_count << std::endl;                        

    // read two files of words and display the words which are common to both files
    std::string filename_A{"textA"};
    std::string filename_B{"textB"};
    
    std::ifstream file_in_A(filename_A);
    std::ifstream file_in_B(filename_B);
    if (file_in_A && file_in_B) {
        std::istream_iterator<std::string> is_it_A(file_in_A);
        std::istream_iterator<std::string> is_it_B(file_in_B);
        std::istream_iterator<std::string> eof;

        std::set<std::string> words_A(is_it_A, eof);
        std::set<std::string> words_B(is_it_B, eof);

        std::ostream_iterator<std::string> console(std::cout, ", ");

        std::set_intersection(words_A.begin(), words_A.end(),
                              words_B.begin(), words_B.end(),
                              console);

        file_in_A.close();
        file_in_B.close();
    } else {
        std::cout << "> no such file '" << filename_A << "' or '" << filename_B << "'" << std::endl;
    }

    return 0;
}
