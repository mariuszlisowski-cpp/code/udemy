/*  Rule of Three:
    - destructor
    - copy constructor
    - copy assignment operator
*/
#include <iostream>
#include <memory>

class Foo {
};

class Bar {
public:
    Bar() {
        // foo_ = new Foo();
        foo_.reset(new Foo());                                              // create a new object
        std::cout << "> creating Bar at: " << this << std::endl;
    }
    ~Bar() {
        // delete foo_;                                                     // no need to delete
        std::cout << "> deleting Bar at: " << this << std::endl;
    }

    std::shared_ptr<Foo>& get_pointer() {
        return foo_;
    }

private:
    // Foo* foo_;
    std::shared_ptr<Foo> foo_;
};

int main() {
    Bar original;
    Bar copy{ original };                                                   // NOT copyable if unique_ptr
    std::cout << "> count: " << original.get_pointer().use_count() << '\n'; // two pointers
    std::cout << "> count: " << copy.get_pointer().use_count() << '\n';     // but one object exist

    return 0;
}                                                                           // Foo object destructed properly
