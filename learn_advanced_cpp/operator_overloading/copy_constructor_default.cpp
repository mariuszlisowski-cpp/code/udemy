#include <iostream>
#include <string>

class Test {
public:
    Test(int id, std::string name) : id_(id), name_(name) {
        ptr_ = new int(99);
    }
    ~Test() {
        delete ptr_;                                                // double deleted memory (ERROR)
    }

    void print_data() const {
        std::cout << id_ << ": " << name_ << std::endl;
    }
    void print_pointer() const {
        std::cout << "pointer value: " << *ptr_ << ", ";            // pointer value (same)
        std::cout << "pointing to: " << ptr_ << ", ";               // address inside the pointer (same - shallow copy)
        std::cout << "pointer address: " << &ptr_ << std::endl;     // address of the pointer (different)
    }

private:
    int id_;
    int* ptr_;
    std::string name_;
};

int main() {
    Test testA(77, "Me");
    Test testB = testA;                                             // implicit call of a copy constructor (default)

    testA.print_data();
    testB.print_data();                                             // data deep copied

    testA.print_pointer();                                          // pointer shallow copied (different pointers
    testB.print_pointer();                                          // pointing to the same address - BAD)

    return 0;
}
