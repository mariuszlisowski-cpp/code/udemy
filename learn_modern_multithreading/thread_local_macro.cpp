#include <iostream>
#include <random>
#include <thread>

using namespace std::literals;

// convenience macro
thread_local std::mt19937 mt;                                   // not seeded generator (new for every thread)
std::mt19937 mt_shared;                                         // thread shared (unique values for all threads)

void generate() {
    std::uniform_real_distribution<double> dist(0, 1);

    for (int i{0}; i < 5; ++i) {
        std::cout << dist(mt) << ' ';
    }
    std::cout << std::endl;
}

void generate_shared() {
    std::uniform_real_distribution<double> dist(0, 1);

    for (int i{0}; i < 5; ++i) {
        std::cout << dist(mt_shared) << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::thread th1(generate);    
    th1.join();

    std::thread th2(generate);    
    th2.join();

    std::this_thread::sleep_for(1s);

    std::thread th3(generate_shared);
    th3.join();

    std::thread th4(generate_shared);
    th4.join();

    return 0;
}
