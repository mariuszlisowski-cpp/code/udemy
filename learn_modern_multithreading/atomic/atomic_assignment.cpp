#include <atomic>
#include <iostream>

void test() {
    std::atomic<int> x{};
    x = 7;                                                  // atomic assignment to x
    int y = x;                                              // atomic assignment from x
}

int main() {

    return 0;
}
