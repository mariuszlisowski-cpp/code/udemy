#include <iostream>
#include <thread>

std::atomic<int> counter{};

int test() {
    for (size_t i{0}; i < 100'000; ++i) {
        ++counter;                                              // atomic operation (no interruption)
    }

    return counter;
}

int main() {
    std::thread th1(test);
    std::thread th2(test);
    std::thread th3(test);

    th1.join();
    th2.join();
    th3.join();

    std::cout << counter << std::endl;

    return 0;
}
