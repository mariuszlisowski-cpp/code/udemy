#include <iostream>
#include <iterator>

int main() {
    char array[]{"abcd"};
    size_t size = sizeof(array) / sizeof(*array);

    char* c_ptr = array;
    std::cout << *c_ptr << std::endl;
    std::cout << *++c_ptr << std::endl;     // prefix (increment first then dereference)
    std::cout << *(++c_ptr) << std::endl;   // same as above (as ++ has higher precedence than *)
    std::cout << *c_ptr++ << std::endl;     // postfix (dereference first then increment)
    std::cout << *c_ptr << std::endl;

    c_ptr = array;
    for (size_t i = 0; i < size; ++i) {
        std::cout << ++(*c_ptr++) << ' ';   // increment dereferenced pointer
        // c_ptr++;                         // done above already
    }

    return 0;
}
