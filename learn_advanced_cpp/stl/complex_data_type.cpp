#include <iostream>
#include <map>
#include <vector>

int main() {
    std::map<std::string, std::vector<int>> scores;

    scores["Mike"].push_back(1);                                    // adding a new key & pushing to a vector
    scores["Elen"].push_back(2);                                    // same as above
    scores["Gary"].push_back(4);                                    // saa
    scores["Gary"].push_back(3);                                    // key exists thus adding to a vector
    scores["Elen"].push_back(2);                                    // saa

    scores.insert_or_assign( "Mike", std::vector<int>{6, 6, 6} );   // replace if the key exists or creates a new one
    scores.insert( {{"Mike"}, {4, 5, 5}} );                         // does nothing if the key exists 
    scores.insert( {{"Miki"}, {4, 5, 5}} );                         // creates a new key as no such key exists 

    for (auto& score : scores) {
        std::string name = score.first;
        std::cout << name << ": ";
        std::vector<int> points = score.second;
        for (auto point : points) {
            std::cout << point << ' ';
        }
        std::cout << std::endl;
    }

    return 0;
}
