#include <algorithm>
#include <iostream>
#include <random>

template<typename T>
void print_random_int(int from, int to, size_t count) {
    static std::random_device rd;
    static std::mt19937 mt(rd()); // random_device as a seed (most common use)
    
    std::uniform_int_distribution<T> dist_int(from, to);; // inclusive range    
    for (size_t i = 0; i < count; ++i) {
        std::cout << dist_int(mt) << ' ';
    }
    std::cout << std::endl;
}

int main() {

    print_random_int<size_t>(1, 9, 10);

    return 0;
}
