#pragma once

#include <string>

class Customer {
public:
    Customer(std::string name) : name_(name) {}

    std::string name() {
        return name_;
    }

private:
    std::string name_;
};
