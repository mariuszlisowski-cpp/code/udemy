#ifndef CONCURRENT_QUEUE_HPP
#define CONCURRENT_QUEUE_HPP

#include <condition_variable>
#include <exception>
#include <iostream>
#include <queue>
#include <mutex>
#include <string>

class concurrent_queue_exception : public std::exception {
public:
    concurrent_queue_exception(const std::string& message) :
        message_(message) {}
    
    const char* what() const noexcept override {
        return message_.c_str();
    }
    
private:
    std::string message_;
};

template <typename T>
class concurrent_queue {
public:
    concurrent_queue() = default;

    void push(const T& value) {
        std::lock_guard<std::mutex> lg(m_);
        q_.push(value);
        cv_.notify_one();                               // condition_variable notified when pushed
                                                        // so the queue could be read
    }

    T pop() {
        std::unique_lock<std::mutex> ul(m_);
        cv_.wait(ul, [this]{ return !q_.empty(); });    // waiting for pushing onto the queue (with predicate)
        T value = q_.front();                           // now the queue can be read
        q_.pop();

        return value;
    }

private:
    std::queue<T> q_;
    std::mutex m_;
    std::condition_variable cv_;                        // condition_variable added to avoid reading empty queue
};

#endif
