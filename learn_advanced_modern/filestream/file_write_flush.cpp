#include <fstream>
#include <iostream>

int main() {
    std::ofstream out_stream("hello");

    if (out_stream) {
        for (size_t i = 0; i < 100000; ++i) {
            std::cout << i << std::endl;    // new line flushes the buffer
            out_stream << i << std::flush;  // lowers the performance but keeps data integrity
        }

        out_stream.close();
    }

    return 0;
}
