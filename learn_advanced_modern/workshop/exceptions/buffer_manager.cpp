#include "buffer_manager.hpp"

#include <iostream>
#include <new>

BufferManager::BufferManager(int size) : size(size) {
    // throw std::bad_alloc();
    buffer = new char[size];
}

BufferManager::~BufferManager() {
    delete[] buffer;
    std::cout << "> desctuctor call" << std::endl;
}

BufferManager::BufferManager(const BufferManager& other) {
    // throw std::bad_alloc();
    size = other.size;
    buffer = new char[size];
    for (int i = 0; i < size; ++i) {
        buffer[i] = other.buffer[i];
    }
}

BufferManager& BufferManager::operator=(const BufferManager& other) {
    // throw std::bad_alloc();
    if (&other != this) {
        size = other.size;
        delete [] buffer;
        buffer = new char[size];
        for (int i = 0; i < size; ++i) {
            buffer[i] = other.buffer[i];
        }
    }
    
    return *this;
}

void BufferManager::populate_buffer(char* data) {
    for (int i = 0; i < size; ++i) {
        buffer[i] = data[i];
    }
}

void BufferManager::print_buffer() {
    for (int i = 0; i < size; ++i) {
        std::cout << buffer[i];
    }
    std::cout << std::endl;
}
