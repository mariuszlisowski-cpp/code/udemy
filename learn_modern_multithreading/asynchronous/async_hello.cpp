#include <future>
#include <iostream>
#include <thread>

using namespace std::literals;

// callable object (thread entry point)
void hello() {
    std::this_thread::sleep_for(2s);
    std::cout << "> hello, thread!" << std::endl;
}

int main() {
    // creating a separate thread
    auto future{std::async(hello)};                             // MUST return std::future
                                                                // to be asynchronous
    std::cout << "> waiting to say hello..." << std::endl;

    return 0;
}
