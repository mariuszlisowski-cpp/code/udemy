#include <fstream>
// #include <ios>
#include <iostream>

int main() {
    char filename[] {"hello"};
    std::ofstream out_file(filename, std::ios_base::app); // append mode (std::ofstream::app)

    if (out_file.is_open()) {
        out_file << "hello, world" << std::endl;
        std::cout << "File '" << filename << "' appended successfully!" << std::endl;
        out_file.close();
    }


    return 0;
}
