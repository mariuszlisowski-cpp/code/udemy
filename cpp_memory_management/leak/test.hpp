#ifndef test_hpp
#define test_hpp

#include <iostream>

class Test {
public:
    // constructor
    Test() {
        std::cout << "Constructing Test at " << this << std::endl;
    }
    
    // destructor
    ~Test() {
        std::cout << "Deleting Test at " << this << std::endl;
    }
    
    // copy constructor
    Test(const Test& old) {
        std::cout << "Copy constructing Test " << std::endl;
    }
    
    // copy assignment operator
    Test& operator=(const Test& old) {
        std::cout << "Copy assignment of Test " << std::endl;
        return *this;
    }
    
    // move constructor
    Test(Test&& old) {
        std::cout << "Move constructing Test " << std::endl;
    }
    
    // move assignment operator
    Test& operator=(Test&& old){
        std::cout << "Move assignment of Test " << std::endl;
        return *this;
    }
    
    void talk() {
        std::cout << "Misa talka" << std::endl;
    }
};

#endif /* test_hpp */
