#include "refrigerator.hpp"

#include <fstream>
#include <iostream>

int main() {
    Refrigerator refrigerator(20);
    std::fstream file;
    file.open("log", std::ios::out | std::ios::app);

    if (file) {
        refrigerator.print(std::cout);
        refrigerator.print(file);
        std::cout << "> state logged\n" << std::endl;

        refrigerator.switch_on();
        refrigerator.close_door();
        refrigerator.set_temperature(0.5);

        refrigerator.print(std::cout);
        refrigerator.print(file);
        std::cout << "> state logged\n" << std::endl;

        file.close();
    }

    return 0;
}
