#include <iostream>

int main() {
    int x{1}, y{2}, z{0};
    
    // functor captured current values of the above variables
    auto functor =
        [=, &z]() mutable {
            ++x;    // variable state preserved inside functor
            ++y;    // variable state preserved inside functor
            z = x + y;
            std::cout << "> inside functor\t";
            std::cout << "x: " << x << " | y: " << y << " | z: " << z << std::endl;
        };

    functor();
    std::cout << "> outside functor\t";
    std::cout << "x: " << x << " | y: " << y << " | z: " << z << std::endl;
    
    // variables change not affecting the functor (which has already been initialized)
    x = 5;
    y = 6;
    
    // functor preserves the internal state
    functor();
    std::cout << "> outside functor\t";
    std::cout << "x: " << x << " | y: " << y << " | z: " << z << std::endl;
    
    // thus continuous increment is possible
    functor();
    std::cout << "> outside functor\t";
    std::cout << "x: " << x << " | y: " << y << " | z: " << z << std::endl;

    return 0;
}
