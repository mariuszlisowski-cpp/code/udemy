
#include <iostream>
#include <list>

template <typename T>
void print_list(std::list<T> l) {
    std::cout << "-----" << std::endl;
    for (auto it{ l.begin() }; it != l.end(); ++it) {
        std::cout << *it << std::endl;
    }
}

int main() {
    std::list<int> numbers;
    numbers.push_back(1);
    numbers.push_back(2);
    numbers.push_back(3);
    numbers.push_front(0);

    std::list<int>::iterator it{ numbers.begin() };
    it++;
    auto it_ins = numbers.insert(it, 99);                                           // insert befor iterator
    std::cout << "> inserted: " << *it_ins << std::endl;

    print_list(numbers);

    auto it_erase = numbers.erase(it_ins);                                          // iterator invalidetd (it_ins)
    print_list(numbers);
    std::cout << "> after erased: " << *it_erase << std::endl;                      // valid iterator

    // erase all occurences
    numbers.push_back(3);
    numbers.push_back(3);
    it = numbers.begin();
    while (it != numbers.end()) {
        if (*it == 3) {
            it = numbers.erase(it);
        } else {
            ++it;
        }
    }

    print_list(numbers);

    return 0;
}
