#pragma once

#include <string>

class Employee {
public:
    Employee(std::string name, std::string title, size_t years);

    std::string getName() const;
    std::string getTitle() const;
    size_t getSeniority() const;

    bool operator<(const Employee& rhs) const;

private:
    std::string name_;
    std::string title_;
    size_t seniority_;
};
