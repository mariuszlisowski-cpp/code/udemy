#include <chrono>
#include <iostream>

int main() {
    
    std::chrono::minutes();             // one minute interval
    std::chrono::seconds(2);            // two seconds interval
    std::chrono::milliseconds(20);      // 1/1000
    std::chrono::microseconds(200);     // 1/1000_000

    using namespace std::literals;      // std::chrono_literals

    std::chrono::seconds two_secs{2s};
    std::chrono::milliseconds long_interval{20ms};
    std::chrono::microseconds short_interval{2us};

    return 0;
}
