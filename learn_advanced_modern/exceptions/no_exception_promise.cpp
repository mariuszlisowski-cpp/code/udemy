#include <iostream>
#include <stdexcept>

// c++11 (not enforced by a compiler)
void does_not_throw() noexcept {
    std::cout << "> I promise not to throw an exception" << std::endl;
}

// earlier versions (not enforced by a compiler)
void does_not_throw(int) throw() {
    std::cout << "> I promise not to throw an exception" << std::endl;
}

/* not allowed in c++17
void throw_exceptions() throw(std::invalid_argument, std::out_of_range) {} */

int main() {
    does_not_throw();
    does_not_throw(1);

    return 0;
}
