#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

template <class T>
void print_container(const std::vector<T>& cont) {
    std::for_each(cont.begin(), cont.end(),
                  [](const T& s) { std::cout << s << ' '; });
    std::cout << std::endl;
}

int main() {
    constexpr size_t size = 2;
    std::vector<std::string> v; // no elements inside

    std::fill_n(std::back_inserter(v), size * 2, "safe"); // safe (vector increases the size)
    print_container(v);

    // vector of double size now thus safe
    auto it = std::fill_n(v.begin(), size, "danger"); // danger (out of range if wrong size)
    print_container(v);
    std::cout << *it << std::endl;
    
    return 0;
}
