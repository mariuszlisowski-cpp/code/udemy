#include <iostream>
#include <vector>

class Foo {
public:
    Foo(int value) : value_(value) {
        std::cout << "# constructing" << std::endl;
    }
    ~Foo() {
        std::cout << "# destructing" << std::endl;
    }
private:
    int value_;
};

int main() {
    /* vector on stack */
    {
        std::vector<Foo*> v_ptrs(2);
        v_ptrs[0] = new Foo(7);
        v_ptrs[1] = new Foo(9);

        for (auto& ptr : v_ptrs) {
            delete ptr;                                             // need to release memory
            ptr = nullptr;                                          // be safe & make null
        }

        v_ptrs.clear();                                             // vector may be emptied
    }                                                               // going out of scope

    std::cout << "> exitting" << std::endl;
    
    return 0;
}
