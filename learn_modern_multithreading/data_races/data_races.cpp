#include <iostream>
#include <string>
#include <thread>

using namespace std::literals;

void print(std::string str) {
    size_t intervals{3};
    for (size_t i = 0; i < intervals; ++i) {
        if (str.size() == 3) {
            std::cout << str[0] << str[1] << str[2] << std::endl;
            std::this_thread::sleep_for(50ms);
        }
    }
}

int main() {
    std::thread t1{print, "abc"};
    std::thread t2{print, "def"};
    std::thread t3{print, "ghi"};

    t1.join();
    t2.join();
    t3.join();

    return 0;
}
