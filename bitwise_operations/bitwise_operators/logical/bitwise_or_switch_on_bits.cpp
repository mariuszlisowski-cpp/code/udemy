/* switch ON a single bit - inclusive OR - symbol | */

#include <bitset>
#include <cstdint>
#include <iostream>

/* index    7  6  5  4 3 2 1 0th
   power  2^7                2^0
   weight 128 64 32 16 8 4 2 1

   valueA 00000100
          OR
   valueB 00000010
          OR
   valueC 00000001
          ________
   result 00000111
    
   truth table:
   0 OR 0 -> 0       both false -> 0
   0 OR 1 -> 1
   1 OR 0 -> 1
   1 OR 1 -> 1
*/

int main() {
    /* binary */
    uint8_t valueA{0b00000100};                                // 4
    uint8_t valueB{0b00000010};                                // 2
    uint8_t valueC{0b00000001};                                // 1

    uint8_t result = valueA | valueB | valueC;
    std::cout << std::bitset<8>(result) << std::endl;
     
    /* decimal */
    std::cout << std::bitset<8>(4 | 2 | 1) << std::endl;

    return 0;
}
