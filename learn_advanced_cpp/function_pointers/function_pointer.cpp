#include <iostream>

void test() {
    std::cout << "Hi!" << std::endl;
}

void funcA(int val, float num) {
    std::cout << "Hello!" << std::endl;
}

double funcB(int val, float num) {
    std::cout << "Hello!" << std::endl;

    return val + num;
}

int main() {
    test();                                 // regular function call

    void (*pTestA)();                       // function prototype (returnig nothing)
    pTestA = &test;                         // poinging to a function now
    pTestA = test;                          // no ampersand needed as the name is actually a pointer

    pTestA();                               // using a pointer (calling a function)
    (*pTestA)();                            // saa
    //*pTestA();                            // ERROR: dereferencing return value

    /* or simply */
    void (*pTestB)() = test;                // prototype and assignment
    pTestB();                               // call
        
    /* with arguments */
    void (*pFuncA)(int, float) = funcA;     // prototype and assignment
    pFuncA(7, 3.14);                        // call

    /* and with a return type */
    double (*pFuncB)(int, float) = funcB;   // prototype and assignment
    auto result = pFuncB(7, 3.14);          // call

    return 0;
}
