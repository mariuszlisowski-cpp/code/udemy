#include <iostream>
#include <mutex>
#include <string>
#include <thread>

using namespace std::literals;

std::mutex print_mutex;

void print(std::string str) {
    size_t intervals{3};
    for (size_t i = 0; i < intervals; ++i) {
        if (str.size() == 3) {
            std::lock_guard<std::mutex> lg(print_mutex);            // RAII wrapper (no need to unlock mutex)
            std::cout << str[0] << str[1] << str[2] << std::endl;   // critical section
            std::this_thread::sleep_for(50ms);                      // still locked here (but not a critical section)
        }
    }                                                               // mutex released (out of scope)
}

int main() {
    std::thread t1{print, "abc"};
    std::thread t2{print, "def"};
    std::thread t3{print, "ghi"};

    t1.join();
    t2.join();
    t3.join();

    return 0;
}
