#include <iostream>
#include <vector>


int main() {
    std::vector<const char*> cstrings{"Hello", ", ", "world!"};

    std::vector<const char*>::const_iterator it = cstrings.begin();
    for (; it != cstrings.end(); ++it) {
        std::cout << *it;
    }

    return 0;
}
