#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

int main() {
    std::vector<int> vec(10);
    const int first_element = -5;
    
    // populate the container
    std::iota(vec.begin(), vec.end(), first_element);

    std::for_each(vec.begin(), vec.end(),
                  [](int val) { std::cout << val << ' '; });

    return 0;
}
