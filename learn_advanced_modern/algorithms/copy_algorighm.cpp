#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print_vector(const std::vector<T> vec) {
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(std::cout, " "));
}

int main() {
    std::vector<int> in {-1, 2, 3, 4, 5};
    std::vector<int> out(in.size()); // sufficient size must be set

    // algorithm
    std::copy(in.begin(), in.end(), out.begin()); // danger: result must have sufficient size
    print_vector(out);

    std::vector<int> out_safe;
    std::copy(in.begin(), in.end(), std::back_inserter(out_safe)); // safe
    print_vector(out_safe);

    // copy constructor (simplest)
    std::vector<int> copied(out);
    print_vector(copied);



    return 0;
}
