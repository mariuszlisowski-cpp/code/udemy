#include <chrono>
#include <iostream>
#include <string>
#include <thread>

int main() {
    std::thread th([]{
        for (size_t i = 0; i < 5; ++i) {
            std::cout << i + 1 << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(250));
        }
    }); // callable object as a lambda

    th.join();

    return 0;
}
