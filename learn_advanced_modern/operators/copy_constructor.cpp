#include <iostream>

class Clazz {
public:
    Clazz() {}
    Clazz(int value) : value_(value) {}
    // same as compiler generated copy constructor (would be good enough here)
    Clazz(const Clazz& other) : value_(other.value_) {
        std::cout << "> copy constructor used" << std::endl;
    }    

    // same as compiler generated assignment operator (would be good enough here)    
    Clazz& operator=(const Clazz& other) {
        // if self-assignment do nothig
        if (this != &other) {
            value_ = other.value_;
            std::cout << "> assignment operator used" << std::endl;
        }

        return *this;
    }

    int getValue() {
        return value_;
    }
private:
    int value_;
};

int main() {
    Clazz c1(9);
    Clazz c2(c1);   // copy constructor used
    std::cout << c2.getValue() << std::endl;

    Clazz c3 = c1;  // copy constructor used (because new instance created)
    c2 = c1;        // assignment operator used here
    std::cout << c3.getValue() << std::endl;

    return 0;
}
