#include <chrono>
#include <iostream>
#include <string>
#include <thread>

using namespace std::literals;  // from c++14

int main() {
    std::thread th([]{
        for (size_t i = 0; i < 5; ++i) {
            std::cout << i + 1 << std::endl;
            std::this_thread::sleep_for(250ms); // literal (equivalent to 250000us microseconds)
        }
    }); // callable object as a lambda

    th.join();

    return 0;
}
