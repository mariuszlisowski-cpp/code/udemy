#include <initializer_list>
#include <iostream>
#include <vector>

class Test {
public:
    Test(std::initializer_list<std::string> texts) {
        for (const auto& text : texts) {
            std::cout << text << ' ';
        }
        std::cout << std::endl;
    }

    void print(std::initializer_list<int> nums) {
        for (auto num : nums) {
            std::cout << num << ' ';
        }
        std::cout << std::endl;
    }
};

int main() {
    std::vector<int> nums{ 1, 2, 3, 4, 5 };                 // curly brackets initialization

    Test test{                                              // initializer list
        "one", "two", "three", "four"                       // c'ctor argument
    };

    test.print({ 1, 2, 3, 4, 5, 6 });                       // initializer list as an argument


    return 0;
}
