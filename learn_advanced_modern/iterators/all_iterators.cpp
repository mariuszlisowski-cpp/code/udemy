#include <array>
#include <iostream>

int main() {
    std::array<int, 5> arr{1, 2, 3 ,4 ,5};

    // const forward
    std::array<int, 5>::const_iterator c_it = arr.cbegin();
    while (c_it != arr.end()) {
        std::cout << *c_it << ' ';
        ++c_it;
    }
    
    // forward
    std::cout << std::endl;
    std::array<int, 5>::iterator it = arr.begin();
    while (it != arr.end()) {
        *it *= 2;                   // doubles values
        std::cout << *it << ' ';
        ++it;
    }
    std::cout << std::endl;

    // const reverse
    std::array<int, 5>::const_reverse_iterator cr_it = arr.crbegin();
    while (cr_it != arr.crend()) {
        std::cout << *cr_it << ' ';
        ++cr_it;
    }
    std::cout << std::endl;

    // reverse
    std::array<int, 5>::reverse_iterator r_it = arr.rbegin();
    while (r_it != arr.rend()) {
        *r_it *= 2;                   // doubles values
        std::cout << *r_it << ' ';
        ++r_it;
    }
    std::cout << std::endl;

    return 0;
}
