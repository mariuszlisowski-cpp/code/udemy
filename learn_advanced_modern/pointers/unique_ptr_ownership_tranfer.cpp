#include <iostream>
#include <memory>
#include <string>

int main() {
    std::unique_ptr<int> uptrA{std::make_unique<int>(77)};
    std::unique_ptr<int> uptrB;

    std::cout << (uptrA ? std::to_string(*uptrA) : "nullptr") << ' '
              << (uptrB ? std::to_string(*uptrB) : "nullptr") << std::endl;

    // member version
    uptrA.swap(uptrB);

    std::cout << (uptrA ? std::to_string(*uptrA) : "nullptr") << ' '
              << (uptrB ? std::to_string(*uptrB) : "nullptr") << std::endl;

    // non-member version
    std::swap(uptrA, uptrB); // or swap(uptrB, uptrA)

    std::cout << (uptrA ? std::to_string(*uptrA) : "nullptr") << ' '
              << (uptrB ? std::to_string(*uptrB) : "nullptr") << std::endl;

    return 0;
}
