#include <functional>
#include <future>
#include <iostream>
#include <thread>

using namespace std::literals;

void producer(std::promise<int>& p_value) {
    std::cout << "> setting a value..." << std::endl;
    std::this_thread::sleep_for(1s);
    p_value.set_value(9);                               // updating a shared value
}

void consumer(std::shared_future<int>& f_value) {
    int x{f_value.get()};                               // wait until value is updated by promise
    std::cout << "> value consumed" << std::endl;
}

int main() {
     std::promise<int> promise;
     std::shared_future<int> future{promise.get_future()};     // associated future

    std::thread consumeA(consumer, std::ref(future));
    std::thread consumeB(consumer, std::ref(future));
    std::thread consumeC(consumer, std::ref(future));
    std::thread produce(producer, std::ref(promise));

    consumeA.join();
    consumeB.join();
    consumeC.join();
    produce.join();

    return 0;
}
