#include <iostream>

class Test {
public:
    Test() = default;                                       // using default values
    Test(int id, const char* name)
        : id_(id), name_(name) {}
    Test(const Test& other) = default;                      // using default implementation
    Test(Test&& other) = delete;                            // move c'ctor cannot be used
    Test& operator=(const Test& other) = default;           // using default implementation

    void print() {
        std::cout << id_ << " : " << name_ << std::endl;
    }

private:
    int id_{ 99 };                                          // default values
    std::string name_{ "nobody" };                          // every object initial state
};

int main() {
    Test testA;
    testA.print();

    Test testB{ 77, "me" };                                 // defaults override
    testB.print();

    Test testC = testB;                                     // using default copy c'ctor
    testC.print();
    testC = testA;                                          // using default assignment operator
    testC.print();

    // Test testD{ std::move(testC) };                      // move c'ctor explicitly deleted

    return 0;
}
