/* ways to create shared pointer */
#include <memory>
#include <iostream>

struct Foo {
    Foo() {}
    Foo(int length, double radius)
        : length_(length), radius_(radius) {}

    int length_{};
    double radius_{};
};

struct Bar {};

int main() {
    std::shared_ptr<Foo> first_ptr{ std::make_shared<Foo>() };
    std::cout << "> count: " << first_ptr.use_count() << std::endl;

    Foo* raw = first_ptr.get();                                      // get raw ptr
    auto length{ first_ptr->length_ };                               // use struct members

    first_ptr.reset();                                               // delete memory if the last reference
    if (!first_ptr) {                                                // nullptr
        std::cout << "> object deleted" << std::endl;
    }
    if (raw) {                                                      // raw still exist but is invalid
        std::cout << "> invalid pointer!" << std::endl;
    }
    raw = nullptr;                                                   // be SAFE and make it null!

    first_ptr.reset(new Foo());                                      // reuse existing ptr
                                                                     // just one ptr exists pointing to the object
    std::shared_ptr<Foo> second_ptr{ first_ptr };
    std::shared_ptr<Foo> third_ptr{ second_ptr };
    std::shared_ptr<Foo> fourth_ptr{ third_ptr };
    
    /* all ptrs pointing to the same object */
    std::cout << "> count: " << first_ptr.use_count() << '\n';
    std::cout << "> count: " << second_ptr.use_count() << '\n';
    std::cout << "> count: " << third_ptr.use_count() << '\n';
    std::cout << "> count: " << fourth_ptr.use_count() << '\n';

    first_ptr.reset();                                               // any ptr can be deleted to decrease counter
    std::cout << "> count: " << fourth_ptr.use_count() << '\n';      // any ptr here can be used to get
                                                                     // the same value
    return 0;
}
