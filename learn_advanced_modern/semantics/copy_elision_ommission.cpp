/* copy constructor elision
   ~ The compiler is allowed to omit copy constructor calls to optimize the code,
   even if it changes the behaviour of the program
  
   -fno-elide-constructors
   disables copy elision while compiling */

#include <iostream>

class X {
public:
    X() {}

    // copy constructor with side effect (changing stream)
    X(const X& other) {
        std::cout << "> copy made"; // ommited
    }
};

X func() {
    return X();     // should be returned as a copy (actually copy is elided, i.e. ommited)
}                   // RVO (return value optimization)

int main() {
    X x = func();   // should be copied to a new object (actually copy is elided, i.e. ommited)

    return 0;
}
