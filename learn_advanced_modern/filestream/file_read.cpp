#include <fstream>
#include <iostream>
#include <string>

std::string file_read(std::string filename) {
    std::ifstream in_file(filename);

    std::string output;
    if (in_file) {
        // reads chars till whitespace (newline as well)
        // returns false if nothing to read
        while (in_file >> output) {
            std::cout << output << ' ';
        }
        in_file.close();
    } else {
        return "No such file!";
    }

    return output;
}

int main() {
    std::cout << file_read("text") << std::endl;

    return 0;
}
