#include <iostream>
#include <memory>                                                   // for smart pointers

class Test {
public:
    Test() {
        std::cout << "> object created" << std::endl;
    }
    ~Test() {
        std::cout << "< object destroyed" << std::endl;
    }

    void greet() {
        std::cout << "# Hi" << std::endl;
    }
};

const int SIZE{ 3 };

class Temp {
public:
    Temp() : ptr(new Test[SIZE]) {}                                 // no need for a destructor
    
private:
    std::unique_ptr<Test[]> ptr;
};

int main() {
    // additional scope
    {
        Temp temp;                                                  // creating an array of objects

    }                                                               // object going out of scope
                                                                    // thus the whole array is destroyed
    std::cout << "> exitting" << std::endl;
   
    return 0;
}
