#include <iostream>
#include <string>

class Test {
public:
    Test() {}
    Test(int id, std::string name) : id_(id), name_(name) {}
    // copy constructor
    Test(const Test& other) {
        std::cout << "> copy c'tor" << std::endl;
        *this = other;                                                          // copy assignmet call
    }

    // copy assignment operator
    const Test& operator=(const Test& other) {
        std::cout << "> copy assignment" << std::endl;
        if (this != &other) {
            id_ = other.id_;
            name_ = other.name_;
        }
        return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, const Test& test);

private:
    int id_{};
    std::string name_;
};

// stream (left bit shift) operator
std::ostream& operator<<(std::ostream& os, const Test& test) {
    os << test.id_ << ": " << test.name_;
    
    return os;
}

int main() {
    Test testA(77, "Me");
    Test testB = testA;                                                         // copy constructor call

    Test testC;
    testC = testA;                                                              // copy assignmet call

    std::cout << testA << std::endl;                                            // stream operator call

    return 0;
}
