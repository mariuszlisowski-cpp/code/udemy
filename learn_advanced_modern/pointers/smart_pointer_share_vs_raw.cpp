#include <iostream>
#include <memory>

int main() {
    // raw pointers shared
    int* ptrA;
    ptrA = new int{9};
    int* ptrB = ptrA; // A & B points to the memory location with value 9

    std::cout << "> same raw ptr: " << *ptrA << std::endl;
    std::cout << "> same raw ptr: " << *ptrB << std::endl;
    delete ptrA; // or ptrB (same effect)

    // smart pointers shared
    std::shared_ptr<int> sh_ptrA;
    sh_ptrA = std::make_shared<int>(9);
    std::cout << "> shared pointers:" << std::endl;
    {
        // sh_ptrA & sh_ptrB points to the same location
        std::shared_ptr<int> sh_ptrB = sh_ptrA;               // will go out od scope
        std::cout << sh_ptrA.use_count() << ' '               // count 2
                  << sh_ptrB.use_count() << std::endl;        // count 2
        {
            // all points to the same location
            std::shared_ptr<int> sh_ptrC = sh_ptrB;           // will go out od scope
            std::cout << sh_ptrA.use_count() << ' '           // count 3
                      << sh_ptrB.use_count() << ' '           // count 3
                      << sh_ptrC.use_count() << std::endl;    // count 3

        }

    }
    std::cout << sh_ptrA.use_count() << std::endl;            // count 1
    
    // unique pointer
    std::unique_ptr<int> u_ptrA(new int{9});                  // memory allocated
    u_ptrA = std::make_unique<int>(8);                        // memory released + new allocation
    
    std::unique_ptr<int> u_ptrB;
    u_ptrB = std::move(u_ptrA);                               // A nullptr

    std::cout << "> unique pointers:" << std::endl;
    std::cout << *u_ptrB << std::endl;

    return 0;
}
