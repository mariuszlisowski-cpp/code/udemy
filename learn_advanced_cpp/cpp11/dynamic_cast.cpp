#include <iostream>

class Parent {
public:
    virtual void speak() {                                  // make a class plimorphic by adding virtual
        std::cout << "speaking" << std::endl;
    }
};

class Brother : public Parent {
};

class Sister : public Parent {
};

int main() {
    Parent parent;
    Brother brother;

    Parent* pParent{ &parent };                             // sub-class object
    Brother* pBrotherA{ static_cast<Brother*>(pParent) };   // UNSAFE
                                                            // unsafe as child class may lack parent's class methods
    Brother* pBrotherB{ dynamic_cast<Brother*>(pParent) };  // SAFE (checks types)
                                                            // class must be polimorphic
    if (pBrotherB == nullptr) {
        std::cout << "> invalid cast" << std::endl;
    }

    return 0;
}
