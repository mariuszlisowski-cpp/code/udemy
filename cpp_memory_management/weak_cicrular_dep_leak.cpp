#include <iostream>
#include <memory>
#include <string>

class Student {
public:
    Student(const std::string& name) : name_(name) {
        std::cout << "> creating " << name_ << std::endl;
    }
    ~Student() {
        std::cout << "> destructing " << name_ << std::endl;
    }

    void setBestFriend(std::shared_ptr<Student> bestFriend) {
        bestFriend_ = bestFriend;
    }

private:
    std::string name_;
    std::shared_ptr<Student> bestFriend_;                           // first reference (ownership)
};

int main() {
    auto alice{ std::make_shared<Student>("Alice") };               // create first reference to an object
    auto robin{ std::make_shared<Student>("Robin") };               // saa

    /* cicrular dependency ERROR */
    alice->setBestFriend(robin);
    std::cout << "> refs: " << alice.use_count() << '\n';
    robin->setBestFriend(alice);                                    // 2 refrences for an object (here: second)
    std::cout << "> refs: " << robin.use_count() << '\n';

    return 0;
}                                                                   // memory leak (no objects destructed)
