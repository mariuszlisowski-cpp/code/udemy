#include <algorithm>
#include <ios>          // boolalpha
#include <iostream>
#include <ostream>
#include <random>

void generate_shuffle(std::mt19937& mt, double percentage = 0.5) {
    static std::bernoulli_distribution b_dist(percentage); // returns bool type only
    for (size_t i = 0; i < 10; ++i) {
        std::cout << std::boolalpha << b_dist(mt) << ' ';  // application
    }
    std::cout << std::endl;
}

int main() {

    // same set every runtime
    std::cout << "> same set:" << std::endl;
    static std::mt19937 mt;
    generate_shuffle(mt);               // default chances 50/50

    // different set every runtime
    std::cout << "> different set:" << std::endl;
    std::random_device rd;
    static std::mt19937 mt_dev(rd());
    generate_shuffle(mt_dev);           // default chances 50/50

    // different set every runtime (percentage)
    std::cout << "> different set percentage:" << std::endl;
    generate_shuffle(mt_dev, 0.075);    // 75% chances to be true

    return 0;
}
