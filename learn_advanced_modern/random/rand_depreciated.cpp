#include <cstdlib>
#include <iostream>

int main() {
    // std::cout << RAND_MAX << std::endl;
    
    std::srand(std::time(0));
    std::cout << rand() << std::endl; // [0, 2_147_483_647]

    std::cout << 1.0 * rand() / RAND_MAX << std::endl; // [0, 1)
   

    return 0;
}
