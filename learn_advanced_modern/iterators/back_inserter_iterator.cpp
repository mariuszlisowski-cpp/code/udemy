#include <iostream>
#include <vector>

int main() {
    std::vector<int> vec{1, 2, 3};

    std::back_insert_iterator<std::vector<int>> bi_it = std::back_inserter(vec);
    *bi_it = 4; // add a value at the back
    
    // will not work as no member 'push_front' in std::vector
    std::front_insert_iterator<std::vector<int>> fi_it = std::front_inserter(vec);
    // *fi_it = 0; // add a value at the front

    for (const auto value : vec) {
        std::cout << value << ' ';
    }

    return 0;
}
