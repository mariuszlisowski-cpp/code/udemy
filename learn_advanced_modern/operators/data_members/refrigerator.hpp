#ifndef REFRIGERATOR_HPP
#define REFRIGERATOR_HPP

#include <iostream>

class Refrigerator {
public:
    Refrigerator(double temp, bool door = true, bool power = false);

    // streams cannot be passed by value as they are not copyable
    std::ostream& print(std::ostream& os) const;

    void close_door();
    void open_door();
    void switch_on();
    void switch_off();
    
    void set_temperature(double temperature);

private:
    double temperature_;
    bool door_open_;
    bool power_on_;
};

#endif REFRIGERATOR_HPP
