#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int function() {
    static int i{5};
    return i++;
}

class Clazz {
public:
    int operator()() {
        return i--;
    }
private:
    int i{4};
};

template <class T> void print_container(const std::vector<T>&);

int main() {
    constexpr size_t size = 5;
    std::vector<int> v(size);

    // using a functor (lambda)
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; }); // static not captured n
    print_container(v);

    // using a function
    std::generate(v.begin(), v.end(), function);
    print_container(v);

    // using a class
    std::generate(v.begin(), v.end(), Clazz());
    print_container(v);

    return 0;
}

template <class T> void print_container(const std::vector<T>& cont) {
    std::for_each(cont.begin(), cont.end(),
                  [](const T& s) { std::cout << s << ' '; });
    std::cout << std::endl;
}
