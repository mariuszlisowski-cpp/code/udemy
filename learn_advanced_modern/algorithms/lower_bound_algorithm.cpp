#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> vec{1, 2, 3, 4, 5, 6, 7, 8, 9};
    constexpr size_t to_be_found = 5;

    // find first element equal to
    auto it_equal_to = std::find(vec.begin(), vec.end(), to_be_found);
    if (it_equal_to == vec.end()) {
        std::cout << to_be_found << " not found!" << std::endl;
    }
    
    // find the first element equal to or greater than
    auto it_equal_greater_than = std::lower_bound(vec.begin(), vec.end(), to_be_found);
    std::cout << "> first equal to or greater than " << to_be_found << ": " << *it_equal_greater_than << std::endl;
    
    std::cout << "> all equal to or greater than " << to_be_found << ":" << std::endl;
    for (auto it = it_equal_greater_than; it != vec.end(); ++it) {
        std::cout << *it << ' ';
    }

    return 0;
}
