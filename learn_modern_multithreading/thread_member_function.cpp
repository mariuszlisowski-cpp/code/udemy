#include <iostream>
#include <string>
#include <thread>

class Greeter {
public:
    void hello() {
        std::cout << "Hello from a member function!" << std::endl;
    }

    void hello_const(const std::string& str) {
        std::cout << str << "Hello from a member function!" << std::endl;
    }

    void hello_mutable(std::string& str) {
        str = "< ";
        std::cout << str << "Hello from a member function!" << std::endl;
    }

    
};

int main() {
    std::string s{""};
    Greeter greeter;

    // threads starting immediately 
    std::thread th1(&Greeter::hello, &greeter);                      // address of the function, address of the object
    std::thread th2(&Greeter::hello_const, &greeter, "> ");          // addresses and an argument
    std::thread th3(&Greeter::hello_mutable, &greeter, std::ref(s)); // addresses and an argument (cast to reference)

    th1.join();
    th2.join();
    th3.join();

    return 0;
}
