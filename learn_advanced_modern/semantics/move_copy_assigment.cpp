#include <iostream>
#include <iterator>
#include <string>

class Point {
public:
    Point() : x_(0), y_(0) {}
    Point(const Point& other) : x_(other.x_), y_(other.x_) {}

    // copy assignment operator
    Point& operator=(const Point& other) {                                // called by Clazz copy assignment operator
        // check for self-assignment
        if (this != &other) {
            x_ = other.x_;
            y_ = other.y_;
            std::cout << "> point copy assignment operator" << std::endl;
        }

        return *this;
    }

    // move assignment operator
    Point& operator=(Point&& other) {                                     // called by Clazz move assignment operator
        // check for self-assignment
        if (this != &other) {
            std::swap(x_, other.x_);
            std::swap(y_, other.y_);
            std::cout << "> point move assignment operator" << std::endl;
        }

        return *this;
    }

private:
    int x_;
    int y_;
};

class Clazz {
public:
    Clazz() : value_(0) {}
    Clazz(int value) : value_(value) {}

    // copy assignment operator
    Clazz& operator=(const Clazz& other) {
        // if self-assignment do nothig
        if (this != &other) {
            value_ = other.value_;
            point_ = other.point_;                                             // copy assignment of Point called
            std::cout << "> clazz copy assignment operator" << std::endl;
        }

        return *this;
    }

    /* move assignment operator
       both other members modified thus an argument cannot be const
       strong exception guarantee as non-throwing (basic if throwing) */
    Clazz& operator=(Clazz&& other) noexcept {
        // check for self-assignment
        if (this != &other) {
            point_ = std::move(other.point_);                                   // move assignment of Point called
                                                                                // cast to rvalue for class members
            std::swap(value_, other.value_);                                    // but swap for build-in types
                                                                                // or regular assign instead
            std::cout << "> clazz move assignment operator" << std::endl;
        }
        return *this; // returns lvalue reference
    }

    int getValue() {
        return value_;
    }

private:
    int value_;
    Point point_;
};

int main() {
    Clazz clazzA;
    Clazz clazzB;

    clazzB = clazzA;                // copy assignment
    clazzB = Clazz();               // move assignment (as temporary xvalue)

    return 0;
}
