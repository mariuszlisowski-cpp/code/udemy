#include <iostream>

// nested
namespace Alpha {
    namespace Beta {
        int value;
    }
}

// or simpler
namespace Alpha::Beta {
    float pi;
}

int hidden{};                                                               // global

// hiding symbols
namespace Alpha {
    int hidden{1};
    namespace Beta {
        int hidden{2};
        void func() {
            std::cout << "> global " << ::hidden << ::std::endl;            // global  called (scope operator alone)
            std::cout << "> alpha " << Alpha::hidden << ::std::endl;
            std::cout << "> beta " << hidden << ::std::endl;                // nearest called
        }
    }
}

int main() {

    Alpha::Beta::value = 5;    
    Alpha::Beta::pi = 3.14;    

    using namespace Alpha::Beta;                                             // valid until END OF FILE
    func();                                                                  // beta function call (not recommended)
    
    int hidden{-1};                                                          // local
    std::cout << hidden << std::endl;                                        // nearest called (local)
    std::cout << Alpha::hidden << std::endl;                                 // alpha called
    std::cout << Alpha::Beta::hidden << std::endl;                           // beta called
    std::cout << ::hidden << std::endl;                                      // global called

    return 0;
}
