#include <algorithm>
#include <iostream>
#include <string>

int main() {
    char char_to_find{'t'};

    // member function
    std::string str{"find a letter"};
    std::string::size_type pos = str.find(char_to_find);
    if (pos != std::string::npos) {
        std::cout << "> found '" << str.at(pos) << "' at position " << pos << std::endl;
    } else {
        std::cout << "> not found!" << std::endl;
    }

    // algorithm
    std::string::iterator it = std::find(str.begin(), str.end(), char_to_find);
    if (it != str.end()) {
        std::cout << "> found '" << *it << "' at position " << std::distance(str.begin(), it) << std::endl;
    }

    it = std::find(++it, str.end(), char_to_find);  // find the same consecutive letter
    if (it != str.end()) {
        std::cout << "> found '" << *it << "' at position " << std::distance(str.begin(), it) << std::endl;
    }

    return 0;
}
