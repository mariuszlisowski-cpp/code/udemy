#include <iostream>
#include <iterator>
#include <map>
#include <unordered_map>

int main() {
	// faster unordered
	std::unordered_map<char, int> un_map;

	/* process the unordered map */

	// slower sorted
	std::map<char, int> map;

	// trick (map will be sorted)
	std::copy(un_map.begin(), un_map.end(), std::inserter(map, map.end())); // no back_inserter for a map!

	return 0;
}
