#include <fstream>
#include <iostream>
#include <string>

;
#pragma pack(push, 1)                                                       // no padding
struct Person {
    char name[50];
    int age;
    double height;
};
#pragma pack(pop)

int main() {
    Person personA{"Elen", 33, 160};                                        // initialized structure
    std::fstream file;                                                      // single in/out object

    // write binary
    std::string filename{"test.bin"};
    file.open(filename, std::ios::binary | std::ios::out);                  // binary out mode
    if (file.is_open()) {
        file.write(reinterpret_cast<char*>(&personA), sizeof(personA));
        file.close();
    } else {
        std::cout << "Error writing a file: " << filename << std::endl;
    }

    // read binary
    Person personB{};
    file.open(filename, std::ios::binary | std::ios::in);                   // bianry in mode
    if (file.is_open()) {
        file.read(reinterpret_cast<char*>(&personB), sizeof(Person));
        file.close();
    } else {
        std::cout << "Error reading a file: " << filename << std::endl;
    }

    std::cout << personB.name << std::endl;

    return 0;
}
