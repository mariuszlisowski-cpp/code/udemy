#include <iostream>
#include <set>

#include "book.hpp"

int main() {
    std::multiset<Book> library;

    library.insert({Book("Stroustrup, Bjarne", "C++ Programming Language")});
    library.insert({Book("Stroustrup, Bjarne", "The C++ Programming Language")});
    library.insert({Book("Stroustrup, Bjarne", "Programming Principles and Practice")}); 
    library.insert({Book("Lippman, Stanley B.", "C++ Primer")});

    for (const auto& book : library) {
        std::cout << book.getAuthor() << " - " << book.getTitle() << std::endl;
    }

    return 0;
}
