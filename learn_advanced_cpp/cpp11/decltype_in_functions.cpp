#include <iostream>

template<typename T>
auto testA(T value) -> decltype(value) {
    return value;
}

template<typename T, typename K>
auto testB(T valueA, K valueB) -> decltype(valueA + valueB) {
    return valueA + valueB;
}

int returning_integer() {
    return 77;
}

// auto returning_auto() -> decltype(returning_integer()) {
auto returning_auto() {
    return returning_integer();
}

int main() {
    auto value{ 7 };
    auto cstring{ "c style string" };
    std::cout << typeid(cstring).name() << std::endl;

    auto cstr { testA("testing") };                                     // const char*
    std::cout << typeid(cstr).name() << std::endl;

    auto str { testA(std::string("testing")) };                         // string
    std::cout << typeid(str).name() << std::endl;

    auto concat { testB(std::string("Hi "), std::string("there")) };    // string
    std::cout << typeid(concat).name() << std::endl;

    auto sum { testB(3.14, 2) };
    std::cout << typeid(sum).name() << std::endl;                       // double

    auto integer = returning_auto();

    return 0;
}
