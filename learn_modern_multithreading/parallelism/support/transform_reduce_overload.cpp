#include <execution>
#include <iostream>
#include <numeric>
#include <vector>

// using namespace std::execution;

int main() {
	std::vector<double> expected{ 0.1, 0.2, 0.3, 0.4, 0.5 };
	std::vector<double> actual{ 0.09, 0.22, 0.27, 0.41, 0.52 };

	auto max_diff = std::transform_reduce(std::begin(expected), std::end(expected), std::begin(actual), 0.0,
                                          [](auto a, auto b) { return std::max(a,b); },   // reduce operation
							              [](auto l, auto r) { return std::fabs(r-l); }); // transform operation 

	std::cout << "Result is " << max_diff << std::endl;
}
