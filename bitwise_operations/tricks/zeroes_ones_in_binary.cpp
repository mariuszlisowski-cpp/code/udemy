#include <bitset>
#include <climits>
#include <iostream>
#include <limits>
#include <sstream>
/*
             f   f   f   f   f   f   f   1      hex
    value 11111111111111111111111111110001      bit
                                         &      value & mask
    mask  00000000000000000000000000000001      << 1
                                        &
    mask  00000000000000000000000000000010      << 1
    mask  ...
          &
    mask  10000000000000000000000000000000      last iteration
                                         1      increase ones
                                        0       increase zeros
          1                                     increase ones
 */

auto get_binary(unsigned decimal) {
    std::ostringstream oss;
    constexpr auto bits{ std::numeric_limits<decltype(decimal)>::digits };
    oss << std::bitset<bits>(decimal);

    return oss.str();
}

auto get_zeroes_ones(unsigned value) {
    /* verbose */
    std::cout << get_binary(value) << std::endl;

    auto bits_in_type{ sizeof(decltype(value)) * CHAR_BIT };
    auto mask{ 1 };

    /* verbose */
    std::cout << get_binary(mask) << std::endl;

    unsigned zeroes{};
    unsigned ones{};
    while (bits_in_type--) {
        if (value & mask) {
            ++ones;
        } else {
            ++zeroes;
        }
        mask <<= 1;
    }

    return std::make_pair(zeroes, ones);
}

int main() {
    auto value{ 0xfffffff1 };
    
    auto result{ get_zeroes_ones(value) };
    std::cout << "zeroes: " << result.first <<
                 ", ones: " << result.second << std::endl;

    return 0;
}
