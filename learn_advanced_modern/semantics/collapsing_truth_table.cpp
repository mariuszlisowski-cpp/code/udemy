/*  collapsing truth table (AND)
    lhs     rhs     result
    &       &       &
    &       &&      &
    &&      &       &
    &&      &&      &&
*/

int main() {
    using RI = int&;    // or typedef int& RI

    int i = 9;
    RI j = i;           // or int& j{i} (reference to int)

    RI& rr = j;         // or int&& rr{j} (such semantics not allowed thus using RI)
                        // rr is a reference to reference to int (but is collapsing to reference to int)

    /*  not allowed
    int&& rr = j; */




    return 0;
}
