#include <iostream>
#include <iterator>     // reverse_iterator
#include <map>
#include <string>  
#include <utility>      // make_pair

int main() {
    std::map<int, std::string> numbers;
    numbers.insert(std::make_pair(1, "one"));
    numbers.insert(std::make_pair(2, "two"));
    numbers.insert(std::make_pair(3, "three"));

    using map_iter = std::map<int, std::string>::iterator;

    std::reverse_iterator<map_iter> numbers_end(numbers.begin());
    std::reverse_iterator<map_iter> numbers_begin(numbers.end());

    for ( ; numbers_begin != numbers_end; ++numbers_begin) {
        std::cout << numbers_begin->first << " : " << numbers_begin->second << std::endl;
    }

    return 0;
}
