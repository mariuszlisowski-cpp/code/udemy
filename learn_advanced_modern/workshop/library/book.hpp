#pragma once

#include <string>

class Book {
public:
    Book(std::string autor, std::string title); 
    bool operator<(const Book& other) const;

    std::string getAuthor() const;
    std::string getTitle() const;
    
private:
    std::string autor_;
    std::string title_;
};
