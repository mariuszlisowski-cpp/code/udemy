#pragma once

#include <memory>
#include <string>

#include "bitmap.hpp"
#include "mandelbrot.hpp"
#include "rgb.hpp"
#include "zoom_list.hpp"

namespace raisin {

class fractal_creator {
public:
    fractal_creator(int width, int height);
    virtual ~fractal_creator();

    void run(const std::string& filename);
    void add_zoom(const zoom& z);
    void add_range(double range_end, const rgb& color);

private:
    void calculate_iteration();
    void calculate_total_iterations();
    void draw_fractal();
    void write_bitmap(const std::string& name);
    void calculate_range_totals();

private:
    int width_{};
    int height_{};
    std::unique_ptr<int[]> histogram_;
    std::unique_ptr<int[]> fractal_;
    bitmap bmp_;
    zoom_list z_list_;
    int total_{};
    std::vector<int> ranges_;
    std::vector<rgb> colors_;
    std::vector<int> range_totals_;
    bool got_first_range{};
};

}
