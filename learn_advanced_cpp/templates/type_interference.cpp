#include <iostream>

template<typename T>
void print(T n) {
    std::cout << n << ", type: " << typeid(T).name() << std::endl;
}

// non-templated version
void print(int n) {
    std::cout << n << std::endl;
}

// no template arguments
template<typename T>
void show() {
    std::cout << T() << std::endl;              // construct object in-place (default init)
}

int main() {
    print<std::string>("hello");                // templated version used
    print<int>(7);                              // saa
    print("hello");                             // saa

    print(99);                                  // non-templated varsion used
    print<>(99);                                // templated version used

    show<double>();                             // must be templated (as no arguments)
    // show();                                  // ERROR

    return 0;
}
