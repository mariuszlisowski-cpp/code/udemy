#include <iostream>
#include <queue>

template<typename T>
void print_top(const std::priority_queue<T>& priority_q) {
    std::cout << "> highest prioroty: " << priority_q.top() << std::endl;
}

template<typename T>
void pop_queue(std::priority_queue<T>& priority_q) {
    while (priority_q.size()) {
        std::cout << priority_q.top() << ' ';
        priority_q.pop();
    }
    std::cout << std::endl;
}

class Record {
public:
    int count;
};

struct Comparator
{
  bool operator()(const Record& lhs, const Record& rhs)
  {
    return lhs.count > rhs.count;
  }
};

int main() {
    // highest priority at the front (container adapter)
    std::priority_queue<int> priority_q; // ordered usint operator< (for build-in types by value)
    priority_q.push(4); // inserted at front (the only element)
    priority_q.push(1); // inserted at the back (lowest so far)
    priority_q.push(5); // highest so far thus inserted at front
    priority_q.push(2); // inserted before back
    priority_q.push(3); // inserted before above element

    print_top(priority_q);

    priority_q.pop(); // remove front element
    print_top(priority_q);

    std::cout << "> size: " << priority_q.size() << std::endl;

    pop_queue(priority_q);
        
    if (priority_q.empty()) {
        std::cout << "> queue is empty now" << std::endl;
    }

    /* front() and back() methods not supported
       iterators not supported either
    for (auto el : priority_q) {} // error */

    std::priority_queue<int> priority_init({5, 4});

    return 0;
}
