#include <iostream>
#include <string>

class Student {
public:
    Student(const std::string& name, size_t id)
        : name_(name)
        , id_(id) {}

    // here two instances are equal if both data members are equal
    bool operator==(const Student& other) const {   // method must be const
        return (this->name_ == other.name_ &&
                this->id_ == other.id_);
    }

    bool operator!=(const Student& other) {
        return !(*this == other);   // reuse of already defined operator==
    }

private:
    std::string name_;
    size_t id_;
};

int main() {
    Student studentA{"Ann", 3345};
    Student studentB{"Ann", 3345};

    std::cout << std::boolalpha << (studentA == studentB) << std::endl;

    return 0;
}
