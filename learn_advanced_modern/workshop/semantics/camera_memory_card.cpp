#include <iostream>

// no resource thus no rule of five (no destructor)
class MemoryCard {
public:
    MemoryCard(int capacity) : capacity_(capacity) {}

    // copying
    MemoryCard(const MemoryCard& other) : capacity_(other.capacity_) {}                // copy constructor
    MemoryCard& operator=(const MemoryCard& other) {                                   // copy assigmnent operator
        if (this != &other) {
            capacity_ = other.capacity_;
        }

        return *this;
    }
    // moving
    MemoryCard(MemoryCard&& other) noexcept : capacity_(std::move(other.capacity_)) {} // move constructor
    MemoryCard& operator=(MemoryCard&& other) {                                        // move assignment operator
        if (this != &other) {
            std::swap(capacity_, other.capacity_);
        }

        return *this;
    }

    // exception class prototype
    class CardFull {};

    void store() {
        if (capacity_ == 0)
            throw CardFull{};
        else
            --capacity_;
    }

private:
    int capacity_;
};

/* rule of five as resource to manage (pointer)
   destructor, copy & move constructors, copy & move assignment operators */
class Camera {
public:
    Camera(MemoryCard* memory_card) : memory_card_(memory_card) {}                   // MemoryCard copy constructor
    ~Camera() {
        delete memory_card_;
    }

    // no copying (maintaining custody of the pointer)
    Camera(const Camera&) = delete;
    Camera& operator=(const Camera&) = delete;

    // moving
    Camera(Camera&& other) noexcept : memory_card_(std::move(other.memory_card_)) {} // MemoryCard move constructor
    Camera& operator=(Camera&& other) noexcept {
        if (this != &other) {
            memory_card_ = std::move(other.memory_card_);                            // MemoryCard move constructor
        }

        return *this;
    }

    void take_picture() {
        memory_card_->store();
    }

    void replace(MemoryCard* memory_card) noexcept {
        delete memory_card_;
        memory_card_ = memory_card;
    }

private:
    MemoryCard* memory_card_;
};

Camera make_camera(int capacity) {
    Camera camera{new MemoryCard(capacity)};

    return camera;
}

int main() {
    const int memory_card_capacity = 10; // images
    const int images_to_take = 21;

    Camera camera = make_camera(memory_card_capacity);
    
    int images = images_to_take;
    while (images) {
        try {
            camera.take_picture();
            std::cout << "smile ";
            --images;
        } catch (MemoryCard::CardFull) {
            std::cout << "\n> memory card is full - replacing..." << std::endl;
            camera.replace(new MemoryCard(memory_card_capacity));
        }
    }
}
