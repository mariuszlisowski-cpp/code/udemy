#include <iostream>

// abstract class (has pure virual method)
class Vehicle {
public:
    /* compiler-generated default destructor is non-virtual (static binding)
    ~Vehicle() {} */
    
    // a must if class is abstract (dynamic binding)
    virtual ~Vehicle() {
        std::cout << "~ vehicle destroyed" << std::endl;    // if not virtual, base part destroyed only
    }                               

    virtual void stop() = 0;
};

class Car : public Vehicle {
public:
    ~Car() {
        std::cout << "~ car destroyed" << std::endl;
    }
    void stop() override {
        std::cout << "> car has stopped" << std::endl;
    }
};

int main() {
    Vehicle* vehicle;
    
    vehicle = new Car();    
    vehicle->stop();

    // destructors called
    delete vehicle;         // the child part destroyed first then the parent part of an object

    return 0;
}
