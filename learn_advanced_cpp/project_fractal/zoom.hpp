#pragma once

namespace raisin {

struct zoom {
    int x_{};
    int y_{};
    double scale_{1.0};

    zoom(int x, int y, double scale) : x_(x), y_(y), scale_(scale) {};
};

}
