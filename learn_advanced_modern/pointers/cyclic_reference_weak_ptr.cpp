#include <iostream>
#include <memory>
#include <string>


class Dog {
public:
    Dog(std::string name) : name_(name) {
        std::cout << "> a dog object is created" << std::endl;
    }
    ~Dog() {
        std::cout << "> a dog object is destroyed" << std::endl;
    }

    void make_friend(const std::shared_ptr<Dog>& dog) {
        friend_ = dog;
    }

    void show_friend() {
        if (!friend_.expired()) {
            std::cout << "My friend is " << friend_.lock()->name_ << std::endl; // lock() creates shared_ptr
        }
    } // created shared_ptr out of scope (decremented)

    std::string get_name() {
        return name_;
    }

private:
    std::string name_;
    std::weak_ptr<Dog> friend_;
};

int main() {
    std::shared_ptr<Dog> dogA{std::make_shared<Dog>("Nero")}  ;
    std::shared_ptr<Dog> dogB{std::make_shared<Dog>("Rex")}  ;
    dogA->show_friend(); // no friends yet

    dogA->make_friend(dogB);
    dogB->make_friend(dogA); // OK, no leak
    dogA->show_friend();
    dogB->show_friend();

    std::cout << "> pointer A count: " << dogA.use_count() << std::endl;
    std::cout << "> pointer B count: " << dogB.use_count() << std::endl;


    return 0;
}
