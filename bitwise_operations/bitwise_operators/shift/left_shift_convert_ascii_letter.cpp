/* convert uppercase ASCII letter to lowercase */

#include <iostream>

/*                integer_to_shift   <<   no_of_positions (unsigned)
        MSB is discarded per shift   <<   LSB is replaced by 0

                    1 0 0 0 0 0 0 1  <<   1 (one left)
    1 discarded  <- 0 0 0 0 0 0 1 0  <-   0 added
                    ^             ^
                  MSB             LSB
    most significant bit  |  least significant bit
*/

int main() {
    /* bitwise shift and OR */
    char uppercase_A{65};
    std::cout << uppercase_A << std::endl;

    char upper_lower_mask{1};                               // not correct yet
    upper_lower_mask <<= 5;                                 // 5th bit is set for lowercase letter

    char lowercase_a = uppercase_A | upper_lower_mask;      // switch ON 5th bit
    std::cout << lowercase_a << std::endl;

    /* classically */
    std::cout << char('A' + ' ') << std::endl;              // space value is 32 (diff between cases)

    return 0;
}
