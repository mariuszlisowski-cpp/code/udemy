#include <iostream>

template<typename T>
void print(T n) {
    std::cout << n << ", type: " << typeid(T).name() << std::endl;
}

int main() {
    print<std::string>("hello");
    print("hello");                 // string (deducted)
    
    print<float>(7.7);              // float
    print(7.7f);                    // float (deducted)

    print(7.7);                     // double (deducted)
    print(7);                       // int (deducted)

    return 0;
}
