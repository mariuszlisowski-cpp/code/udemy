#include <iostream>
#include <set>

class Test {
public:
    Test() = default;                                                       // good practice
    Test(int id, const std::string& name) : id_(id), name_(name) {}

    void print() const {                                                    // does not modify an object
        std::cout << id_ << ": " << name_ << std::endl;
    }

    bool operator<(const Test& other) const {                               // does not modify an object
        if (this != &other) {
            if (name_ == other.name_) {
                return id_ < other.id_;                                     // necessary to insert same names
            } else {
                return name_ < other.name_;
            }
        }

        return false;
    }

private:
    int id_;
    std::string name_;
};

template<typename T>
void print_set(std::set<T> s) {
    for (auto& el : s) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::set<int> numbers;                                                  // sorted unique elements
    numbers.insert(3);
    numbers.insert(2);
    numbers.insert(1);
    numbers.insert(3);

    print_set(numbers);

    std::set<int>::iterator it_found = numbers.find(3);
    if (it_found != numbers.end()) {
        std::cout << "> found: " << *it_found << std::endl;
    }

    std::set<Test> tests;
    tests.insert(Test(77, "Me"));
    tests.insert(Test(66, "He"));
    tests.insert(Test(11, "It"));
    tests.insert(Test(22, "It"));

    for (auto it{ tests.begin() }; it != tests.end(); ++it) {
        it->print();                                                        // iterator returns a const object
                                                                            // thus a method must be const also
    }

    return 0;
}
