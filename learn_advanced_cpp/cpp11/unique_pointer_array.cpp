#include <iostream>
#include <memory>                                                   // for smart pointers

class Test {
public:
    Test() {
        std::cout << "> object created" << std::endl;
    }
    ~Test() {
        std::cout << "< object destroyed" << std::endl;
    }

    void greet() {
        std::cout << "# Hi" << std::endl;
    }
};

int main() {
    // additional scope
    {
        std::unique_ptr<Test[]> pTestArray(new Test[2]);
        pTestArray[0].greet();
        pTestArray[1].greet();
    }                                                               // object going out of scope
                                                                    // thus the whole array is destroyed
    std::cout << "> exitting" << std::endl;
   
    return 0;
}
