#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>


template <class T>
void print_vector(std::vector<T> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> nums{9, 1, 2, 3, 4, 9};

    // removing elements using predicate (to the end)
    auto predicate = [](int n){ return n > 4; };
    auto first_removed = std::remove_if(nums.begin(), nums.end(), predicate);
    
    // output
    for (auto it = nums.begin(); it != first_removed; ++it) {
        std::cout << *it << ' ';
    }
    std::cout << "\n> size: " << nums.size() << std::endl;    // still the same size
    print_vector(nums);                                       // with removed numbers

    // erasing elements
    nums.erase(first_removed, nums.end());

    // output    
    std::cout << "\n> size: " << nums.size() << std::endl;      // proper size now
    print_vector(nums);

    return 0;
}
