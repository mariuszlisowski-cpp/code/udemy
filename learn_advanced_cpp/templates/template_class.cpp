#include <iostream>

template<typename T>
class Test {
public:
    Test(T obj) {
        this->obj = obj;
    }

    void print() {
        std::cout << obj << std::endl;              // stream operator must be defined
    }

private:
    T obj;
};

int main() {
    Test<std::string> testA("text");
    testA.print();

    return 0;
}
