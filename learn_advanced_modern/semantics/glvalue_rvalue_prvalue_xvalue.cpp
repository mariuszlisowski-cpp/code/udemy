/*  
    lvalue  - memory address can be taken (&lvalue) from heap or stack
    rvalue  - cannot take a memory address (literal or temporary)
    
    xvalue  - e(x)piring value (temporary)
    glvalue - (g)eneralized lvalue (can have dynamic type)
    prvalue - (p)ure rvalue (literals)

    glvalue consists of:   lvalue  & xvalue (address or temporary)
    rvalue  consists of:   prvalue & xvalue (literal or temporary)
*/

class X {
public:
    X() {}
};

X func() {
    return X();                     // xvalue (temporary as returned by elision)
}

int main() {
    
    X();                            // xvalue (expiring instance)
    X obj((X()));                   // lvalue(xvalue) (expiring instance)
    
    // rhs temporary (xvalue)
    // or pure rvalue (prvalue)
    X x = func();                   // lvalue, prvalue (literal)
    int a = 9;                      // lvalue, prvalue (literal)
    const int& z = 7;               // lvalue, prvalue (must be const as rhs is a literal - cannot modify literal)
    const float& f = a;             // lvalue, xvalue
                                    // (reference NOT to rhs but to temporary converted to float thus const)
    
    X xA;                           // lvalue (an address on stack)
    X xB(xA);                       // lvalue(lvalue)
    
    // lhs & rhs persistent
    X* x_ptr = &xA;                 // lvalue, lvalue (address of lvalue)
    int& b = a;                     // lvalue, lvalue
    int* ptr = &a;                  // lvalue, lvalue (address of lvalue)
    const char* c = "not prvalue";  // lvalue, lvalue (string literal)
    char first = *"not prvalue";    // lvalue, lvalue (first char dereferenced)

    return 0;
}
