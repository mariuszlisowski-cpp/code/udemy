# script mode: cmake -P <filename>
cmake_minimum_required(VERSION 3.0.0)

message("Hello! CMake here.")

set(NICKNAME " Raisin")
set(AGE 44)

message("Hi" ${NICKNAME} "! You're aged " ${AGE} ".")

# list
set(LIST1 A B C)                                         # value of LIST1: A;B;C
message(${LIST1})                                        # printed without semicolons
message("${LIST1}")                                      # printed with semicolons

# list (same as above)
set(LIST2 D;E;F)                                         # value of LIST2: D;E;F
message(${LIST2})                                        # printed without semicolons
message("${LIST2}")                                      # printed with semicolons

# list of strings (same as above)
set(LIST3 "G" "H" "I")                                   # value of LIST3: G;H;I
message(${LIST3})                                        # printed without semicolons
message("${LIST3}")                                      # printed with semicolons

# single string
set(STRING1 "J K L")                                     # value of STRING1: G K L
message(${STRING1})                                      # whole string printed
message("${STRING1}")                                    # whole string printed

# single string (different)
set(STRING2 "M;N;O")                                     # value of STRING2: M;N;O
message(${STRING2})                                      # printed without semicolons
message("${STRING2}")                                    # whole string printed
