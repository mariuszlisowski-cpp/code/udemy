#include <iostream>

#include "complex_number_class.hpp"

using namespace complex;

int main() {
    Complex c1(2, 3);
    Complex c2 = c1;                                    // copy c'tor used

    std::cout << c1 << std::endl;

    Complex c3;
    c3 = c1;                                            // copy assignment operator used
    std::cout << c2 << ", "
              << c3 << std::endl;

    std::cout << c1 + c2 << std::endl;                  // plus operator used
    std::cout << c1 + c2 + c3 << std::endl;             // chaining plus operators

    std::cout << c1 + 8 << std::endl;                   // first overload of plus operator used
    std::cout << 8 + c1 << std::endl;                   // second overload of plus operator used
    std::cout << 8 + c1 + 5 << std::endl;               // first & second overload used

    if (c1 == c2) {                                     // equality test operator used
        std::cout << "> same complex numbers" 
                  << std::endl;
    }

    if (c1 != c2 + 2) {                                 // non-equality test operator used
        std::cout << "> different complex numbers"
                  << std::endl;
    }

    std::cout << *c1 + *Complex(1, 1) << std::endl;     // dereference operator used

    return 0;
}
