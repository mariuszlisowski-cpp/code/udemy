#include <algorithm>
#include <cctype>
#include <iostream>
#include <iterator>
#include <string>

int main() {
    std::string input{"Input text"};
    std::string output;

    // unary operation 
    std::transform(input.begin(), input.end(), std::back_inserter(output),
        [](const char c) {
            return std::toupper(c);
        });

    std::cout << output << std::endl;

    return 0;
}
