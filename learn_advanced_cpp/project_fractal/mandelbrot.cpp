#include "mandelbrot.hpp"

namespace raisin {

int mandelbrot::get_iterations(double x, double y) {
    std::complex<double> z{};
    std::complex<double> c(x, y);

    int iterations{};
    while (iterations < MAX_ITERATIONS) {
        z = z * z + c;
        if (std::abs(z) > 2) {
            break;
        }
        ++iterations;
    }
  
    return iterations;
}

}
