/* left shift - operator << */

#include <bitset>
#include <cstdint>
#include <iostream>

/*                integer_to_shift   <<   no_of_positions (unsigned)
        MSB is discarded per shift   <<   LSB is replaced by 0

                    1 0 0 0 0 0 0 1  <<   1 (one left)
    1 discarded  <- 0 0 0 0 0 0 1 0  <-   0 added
                    ^             ^
                  MSB             LSB
    most significant bit  |  least significant bit
*/

int main() {
    uint8_t value{0b10000001};

    uint8_t result = value << 1;
    std::cout << std::bitset<8>(result) << std::endl;

    result = value << 8;
    std::cout << std::bitset<8>(result) << std::endl;

    return 0;
}
