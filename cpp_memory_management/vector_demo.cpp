#include <iostream>
#include <vector>

template<typename T>
void print_size_capacity(const std::vector<T>& v) {
    std::cout << "> size: " << v.size() << ", ";
    std::cout << "> capacity: " <<v.capacity() << std::endl;
}

int main() {
    std::vector<int> v1;                                            // emtpy vector
    std::vector<int> v2(4);                                         // vector of zeroed elements
    std::vector<int> v3{2, 3, 5, 7, 11};                            // initializer list
                                                                    // size same as a capacity
    print_size_capacity(v3);
    v3.push_back(13);                                               // size incremented - capacity doubled
    print_size_capacity(v3);

    return 0;
}
