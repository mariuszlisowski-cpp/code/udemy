#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>


template <class T>
void print_vector(std::vector<T> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> nums{1, 2, 3, 4, 5};
    std::vector<int> reversed_nums(nums.size());

    std::reverse_copy(nums.begin(), nums.end(), reversed_nums.begin()); // unsafe
    print_vector(reversed_nums);

    std::vector<int> reversed_again;
    std::reverse_copy(reversed_nums.begin(), reversed_nums.end(),
                      std::back_inserter(reversed_again));              // safe
    print_vector(reversed_again);

    return 0;
}
