#include <fstream>
#include <iostream>

int main() {
    char filename[] {"hello"};
    std::ofstream out_file(filename); // overwrites existing file

    if (out_file) {
        out_file << "hello, world" << std::endl;
        std::cout << "File '" << filename << "' created successfully!" << std::endl;
        out_file.close();
    }

    return 0;
}
