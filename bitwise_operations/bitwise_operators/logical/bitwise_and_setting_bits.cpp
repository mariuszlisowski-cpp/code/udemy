/* set all bits to 1 - AND - symbol & */

#include <bitset>
#include <cstdint>
#include <iostream>

/* index    7  6  5  4 3 2 1 0th
   power  2^7                2^0
   weight 128 64 32 16 8 4 2 1

   value  10001100
          AND
   value  00000000
          ________
   result 00000000
   ~      11111111
    
   truth table:
   0 AND 0 -> 0
   0 AND 1 -> 0
   1 AND 0 -> 0
   1 AND 1 -> 1       both true -> 1
*/

int main() {
    uint8_t value{0b10001100};                                  // register
    
    uint8_t result = value & 0;                                 // zeroed register
    uint8_t all_ones = ~result;                                 // complement (reverse)

    std::cout << std::bitset<8>(all_ones) << std::endl;

    return 0;
}
