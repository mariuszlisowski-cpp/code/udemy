#include <iostream>

int main() {

    // non-const reference (regular)
    int value = 5;
    int& b = value;
    b = 9;
    std::cout << "> I'm non-const int: " << b << std::endl;

    return 0;
}
