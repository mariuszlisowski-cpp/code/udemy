#include <iostream>

int main() {
    size_t counter;
    std::string word_permut{"dcba"};

    // not sorted (descending)
    counter = 0;
    do {
        std::cout << word_permut << ' ';
        ++counter;
    } while (std::next_permutation(word_permut.begin(), word_permut.end()));
    std::cout << "\n> number of permutations without sorting: " << counter << std::endl;
    
    // sorted (in ascending order)
    counter = 0;
    std::sort(word_permut.begin(), word_permut.end()); // all permutations if sorted
    do {
        std::cout << word_permut << ' ';
        ++counter;
    } while (std::next_permutation(word_permut.begin(), word_permut.end()));
    std::cout << "\n> number of permutations after sorting: " << counter << std::endl;

    return 0;
}
