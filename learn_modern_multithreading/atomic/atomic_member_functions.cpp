#include <iostream>
#include <thread>

std::atomic<int> a{};

void test() {
    a.store(9);                                   // atomically replace object's value with its argument
    a = 9;                                        // or using assignment operator

    int x = a.load();                             // atomically return object's value 
    int y = a;                                    // or using assignment operator

    int z = a.exchange(7);                        // atomically replace object's value
                                                  // and return the previous value

    int inc = a.fetch_add(1);                     // atomically add argumnent and return previous value
                                                  // eqivalent to a++
    int dec = a.fetch_sub(1);                     // atomically subtract argumnent and return previous value
                                                  // eqivalent to a--
 
}

int main() {
    std::cout << a << std::endl;

    return 0;
}
