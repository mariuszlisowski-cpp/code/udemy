#ifndef COMPLEX_NUMBER_CLASS_HPP
#define COMPLEX_NUMBER_CLASS_HPP

#include <iostream>

// both header & source within the same namespace
namespace complex {

class Complex {
public:
    Complex();
    Complex(double, double);
    Complex(const Complex&);                        // copy c'tor
    Complex& operator=(const Complex&);             // copy assignment

    std::ostream& print(std::ostream& os) const;    // used by operator<< overload
    Complex add_complex(const Complex& rhs) const;
    Complex add_real(const double rhs) const;
    
    bool operator==(const Complex& other);          // equality test operator
    bool operator!=(const Complex& other);          // non-equality test operator
    Complex operator*() const;                      // dereference operator

  private:
    double real_{};
    double imaginary_{};
};

// out of class signatures (in complex namespace)
std::ostream& operator<<(std::ostream& os, const Complex& complex);
Complex operator+(const Complex& lhs, const Complex& rhs);
Complex operator+(const Complex& lhs, const double rhs);                // first overload
Complex operator+(const double lhs, const Complex& rhs);                // second overload

} // namespace complex

#endif
