#include "../include/division.hpp"      // no need for relative path as target_include_directories exists

float division(float num1, float num2) { return num1 / num2; }
