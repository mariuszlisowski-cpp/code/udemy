#include "test.hpp"

#include <iostream>

void function() {
    throw std::exception();
}

int main() {
    try {
        Test* test{ new Test() };
        test->talk();

        function();                                     // throw an exception

        delete test;                                     // never reached
    } catch (std::exception& e) {
                                                        // resource not freed 
    }

    return 0;
}
