#include <iostream>
#include <string>

class BufferManager {
  public:
    // must have const argument to avoid conversion error from string literal to 'char *'
    BufferManager(const char* cstring) : buffer_(cstring) {
        int i{};
        char c{*buffer_};
        while (c != '\0') {
            ++size_;
            c = *(buffer_ + ++i);    
        }
    }

    friend void swap(BufferManager& first, BufferManager& second) noexcept;

    void print() {
        for (size_t i = 0; i < size_; ++i) {
            std::cout << *(buffer_ + i);
        }
        std::cout << std::endl;
    }

  private:
    int size_{};
    const char* buffer_; // must be const to avoid conversion error from string literal to 'char *'
};

// function should be global and inline
inline void swap(BufferManager& first, BufferManager& second) noexcept {
    using std::swap;                        // if no overloaded version generic swap is used

    swap(first.size_, second.size_);        // overloaded swap may be used for appropriate custom types
    swap(first.buffer_, second.buffer_);    // swapping each data member
}

int main() {
    BufferManager buffA{"1st"};
    BufferManager buffB{"2nd"};
    
    swap(buffA, buffB);
    buffA.print();
    buffB.print();

    return 0;
}
