#include <iomanip>  // setw, left, setfill
#include <iostream>

int main() {
    
    // right justification (default)
    std::cout << std::setw(15) << "Test field"          // setw is not sticky
              << std::setw(10) << "output"              // thus needs to be repeated
              << std::setw(6) << "value" << std::endl;


    // left justification
    std::cout << std::left                              // left is sticky
              << std::setw(15) << "Test field"          // thus no need to repeat
              << std::setw(10) << "output" 
              << std::setw(6) << "value" << std::endl;


    std::cout << std::setfill('_')                      // sticy (padding char)
              << std::left
              << std::setw(15) << "Test field"
              << std::setw(10) << "output" 
              << std::setw(6) << "value" << std::endl;

    return 0;
}
