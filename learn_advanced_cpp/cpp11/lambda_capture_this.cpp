#include <iostream>

class Test {
public:
    void run() {
        int third{ 3 };
        int fourth{ 4 };
        auto function_ptr{ [this, third, fourth] {                      // first & second unaccessible
            std::cout << ++this->first << ++this->second << std::endl;  // must be captured as this (by ref)
            std::cout << third << fourth << std::endl;
        } };
        function_ptr();
    }

private:
    int first{ 1 };
    int second{ 2 };
};

int main() {
    Test test;
    test.run();

    return 0;
}
