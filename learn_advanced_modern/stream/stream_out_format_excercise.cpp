#include <iomanip>
#include <iostream>

int main() {
    
    
    std::cout << std::setfill('.')
              << std::left
              << std::setw(13)
              << "Lions" 
              << std::right
              << std::setw(4) 
              << 4 << std::endl;
    
    std::cout << std::left
              << std::setw(13)
              << "Chimpanzees" 
              << std::right
              << std::setw(4) 
              << 23 << std::endl;
    
    std::cout << std::left
              << std::setw(13)
              << "Wildebeest" 
              << std::right
              << std::setw(4) 
              << 247 << std::endl;
    
    return 0;
}
