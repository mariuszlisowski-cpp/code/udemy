/* ways to create unique pointer */
#include <memory>

struct Foo {
    Foo(int length, double radius)
        : length_(length), radius_(radius) {}

    int length_;
    double radius_;
};

struct Bar {};

int main() {
    /* 1 */
    std::unique_ptr<Foo> u_ptr{ std::make_unique<Foo>(7, 2.89) };       // pass arguments to a constructor

    /* 2 */
    Foo* raw_ptr{ new Foo(1, 3 / .14) };
    std::unique_ptr<Foo> from_raw(raw_ptr);                             // takes ownership of a raw pointer
                                                                        // will delete this pointer later

    /* 3 */
    u_ptr.reset(new Foo(9, 7.9));                                       // reusing existing unique pointer
                                                                        // previous object will be deleted
                                                                        // takes ownership of a raw pointer
    return 0;
}
