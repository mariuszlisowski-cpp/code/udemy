#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

using namespace std::literals;

class Type {
public:
    void do_it() {
        std::cout << "> pointer used by thread " 
                  << std::this_thread::get_id() << std::endl;
    }
};

Type* type{nullptr};                                                // value to be lazy initialized
std::mutex process_mutex;

void process() {
    if (!type) {
        std::lock_guard<std::mutex> lg(process_mutex);

        // double-checked
        if (!type) {
            std::cout << "> pointer lazy initialized by thread " 
                    << std::this_thread::get_id() << std::endl;
            type = new Type();                                      // thread safe since c++17 (no data races)
                                                                    // Order of operations guaranteed:
                                                                    // 1. allocate enough memory
                                                                    // 2. construct an object in a memory
                                                                    // 3. store its address in a pointer
        }
    }
    type->do_it();
}

int main() {
    const unsigned THREADS = 25;
    std::vector<std::thread> threads;
    threads.reserve(THREADS);

    for (size_t i{0}; i < THREADS; ++i) {
        threads.push_back(std::thread(process));
    }
    
    std::this_thread::sleep_for(500ms);

    for (size_t i{0}; i < THREADS; ++i) {
        threads[i].join();
    }

    return 0;
}
