#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template <class T>
void print_vector(std::vector<T> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> nums{1, 2, 3, 4, 5, 6};

    // remove duplicate adjacent elements (using binary predicate)
    auto first_after = std::unique(nums.begin(), nums.end(),
                                   [](int n, int m){
                                       return m == ++n; // e.g. 1 & 2 are duplicated (not default ==)
                                   });
    print_vector(nums);

    nums.erase(first_after, nums.end());                        // removed definitely
    print_vector(nums);

    return 0;
}
