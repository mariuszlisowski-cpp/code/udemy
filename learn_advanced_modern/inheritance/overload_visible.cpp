#include <iostream>

class Vehicle {
public:
    void accelerate() {
        std::cout << "> vehicle accelerating" << std::endl;
    }
    void accelerate(char c) {
        std::cout << "> vehicle " << c << " accelerating" << std::endl;
    }

};

class Aeroplace : public Vehicle {
public:
    // make parent methods visible
	using Vehicle::accelerate;      // all overloaded versions (thus no brackets)

    // method overloaded (hides inherited method)
    void accelerate(int rate) {
        std::cout << "> vehicle accelerating with rate " << rate << std::endl;
    }
};


int main() {
    Aeroplace plane;

    plane.accelerate(3);        // method overloaded
    plane.accelerate();         // inherited method now visible
    plane.accelerate('A');      // inherited method now visible (char version)

    return 0;
}
