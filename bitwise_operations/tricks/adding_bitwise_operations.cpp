#include <iostream>

/*  lhs 9   0000 1001
    rhs 10  0000 1010
 */

int add(int lhs, int rhs) {
    int carry{1};
    while (carry) {

        carry = lhs & rhs;                              // AND 1 & 1 -> 1 as carry
        lhs ^= rhs;                                     // XOR
        rhs = carry << 1;                               // LEFT SHIFT
    }

    return lhs;
}

int main() {
    std::cout << add(9, 10) << std::endl;

    return 0;
}
