#include <iostream>

bool is_power_of_two(int value) {
    return value > 0 && (value & (value - 1)) == 0;
}

int main() {
    constexpr size_t max{ 42 };
    for (size_t num = 1; num <= max; ++num) {
        if (is_power_of_two(num)) {
            std::cout << num << " is power of 2" << std::endl;
        }
    }

    return 0;
}
