#include <thread>
#include <mutex>
#include <iostream>
#include <string>

using namespace std::literals;

std::timed_mutex mutex1, mutex2;                                        // to use try_lock_for() method

void cecil() {
	std::this_thread::sleep_for(10ms);
	bool locked{false};
    while (!locked) {
        std::lock_guard lk(mutex1);		                                // lock mutex1
        std::this_thread::sleep_for(1s);	    
        std::cout << "After you, Claude!" << std::endl;
		locked = mutex2.try_lock_for(5ms);          			        // try to get a lock on mutex2
	}
	if (locked)
		std::cout << "> thread 1 has locked both mutexes" << std::endl;
}

void claude() {
    bool locked{false};
    while (!locked) {
        std::lock_guard lk(mutex2);		                                // lock mutex2
        std::this_thread::sleep_for(1s);	    
        std::cout << "No, after you, Cecil!" << std::endl;
		locked = mutex1.try_lock_for(5ms);			                    // try to get a lock on mutex1
	}
	if (locked)
		std::cout << "> thread 2 has locked both mutexes" << std::endl;
}

int main() {
	std::thread t1(cecil);
	std::this_thread::sleep_for(10ms);
	std::thread t2(claude);

	t1.join();
	t2.join();
}
