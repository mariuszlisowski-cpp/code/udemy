#include <iostream>
#include <string>
#include <vector>

int main() {
    std::vector<std::string> input;

    std::cout << "Enter anything (eof ctrl-d)\n: ";
    std::istream_iterator<std::string> is_it(std::cin); // waits for the first input line
    std::istream_iterator<std::string> eof; // end of file (empty iterator to compare with)

    auto bi_it = std::back_inserter(input);
    // std::back_insert_iterator<std::vector<std::string>> bi_it = std::back_inserter(input);
    
    // read next lines until input is empty (as eof)
    while (is_it != eof) {
        std::cout << ": ";
        bi_it = *is_it; // equvalent to input.push_back(*is_it)
        ++is_it; // move to read next string
    }

    for (const auto& str : input) {
        std::cout << std::endl << str;
    }

    return 0;
}
