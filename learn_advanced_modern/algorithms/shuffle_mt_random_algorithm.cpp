#include <algorithm>
#include <iostream>
#include <ostream>
#include <random>
#include <string>
#include <vector>

template <typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& vec) {
    for (const T& val : vec) {
        out << val << ' ';        
    }
    out << std::endl;

    return out;
}

void generate_shuffle(std::mt19937& mt, std::vector<int>& vec) {
    for (size_t i = 0; i < 5; ++i) {
        std::shuffle(vec.begin(), vec.end(), mt);
        std::cout << vec;
    }
}

int main() {
    std::vector<int> vec{1, 2 , 3 ,4, 5, 6, 7, 8, 9};

    // same set every runtime
    std::cout << "> same set although shuffled:" << std::endl;
    static std::mt19937 mt;
    generate_shuffle(mt, vec);

    // different set every runtime
    std::cout << "> different shuffled set:" << std::endl;
    std::random_device rd;
    static std::mt19937 mt_dev(rd());
    generate_shuffle(mt_dev, vec);

    return 0;
}
