#include <chrono>
#include <iostream>
#include <ratio>
#include <thread>

class ThreadGuard {
public:
    explicit ThreadGuard(std::thread th) : th_(std::move(th)) {}
    ~ThreadGuard() {
        if (th_.joinable()) {
            th_.join();                                         // join thread if necessary
            std::cout << "> thread joined" << std::endl;
        }
    }

    ThreadGuard(ThreadGuard&&) noexcept = default;              // move constructor
    ThreadGuard& operator=(ThreadGuard&&) noexcept = default;   // move assignment
    
    // std::thread is move-only
    ThreadGuard(const ThreadGuard&) = delete;                   // copy constructor
    ThreadGuard& operator=(const ThreadGuard&) = delete;        // copy assignment


private:
    std::thread th_;
};

int main() {
    {
        std::thread th([]{
            for (size_t i = 0; i < 3; ++i) {
                std::cout << i + 1 << ' ';
            }
        });
        ThreadGuard tg(std::move(th));
    }                                                           // thread out of scope
                                                                // desctructor called and thread joined

    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "> job done" << std::endl;

    return 0;
}
