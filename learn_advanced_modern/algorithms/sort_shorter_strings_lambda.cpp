#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

template<class T>
void print_container(const T& container) {
    std::for_each(container.begin(), container.end(),
                  [](auto& el) -> void{
                      std::cout << el << std::endl;
                  }); 
}

void sort_shorter_first(std::vector<std::string> strings) {
    // predicate
    auto is_shorter = [](const std::string& lhs, const std::string& rhs) {
        return lhs.size() < rhs.size();
    };
    // algorithm with predicate (lambda functor here)
    std::sort(strings.begin(), strings.end(), is_shorter);
    print_container(strings);
}

int main() {
    std::vector<std::string> words{"a", "collection" , "of", "words", "with", "varying", "lengths"};

    sort_shorter_first(words);

    return 0;
}
