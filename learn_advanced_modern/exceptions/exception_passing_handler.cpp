#include <exception>
#include <iostream>
#include <stdexcept>
#include <string>

void positive_only(int value) {
    if (value < 0) {
        throw std::invalid_argument("ERROR: negative numbers not allowed!");
    }
}

void throw_exception() {
    try {
        positive_only(-1);                      // exception passed here
    } catch (const std::out_of_range& e) {}     // unsuitable as invalid_argument is thrown, jumping out
}

void test_exception() {
    throw_exception();                          // exception passed here but no handler (catch block), jumping out
}

int main() {
    try {
        test_exception();                       // handler present, executing
    } catch (const std::invalid_argument& e) {
        std::cout << e.what() << std::endl;
    }
    
    return 0;
}
