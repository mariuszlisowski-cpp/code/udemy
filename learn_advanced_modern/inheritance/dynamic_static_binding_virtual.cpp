#include <iostream>
#include <memory>
#include <vector>

class Drawable {
public:
    // must be virtual to call dynamic type object (and destroy it to prevent a memory leak)
    virtual ~Drawable() {
        std::cout << "~ drawable destroyed" << std::endl;
    }
    virtual void draw() const {
        std::cout << "> drawing a shape" << std::endl;
    }
    virtual void draw(int radius) const {}
};

class Circle : public Drawable {
public:
    ~Circle() {
        std::cout << "~ circle destroyed" << std::endl;
    }
    void draw() const override {
        std::cout << "> drawing a circle" << std::endl;
    }
    void draw(int radius) const override {
        std::cout << "> drawing a circle of radius " << radius << std::endl;
    }
};

class Triangle : public Drawable {
public:
    ~Triangle() {
        std::cout << "~ triangle destroyed" << std::endl;
    }
    void draw() const override {
        std::cout << "> drawing a triangle" << std::endl;
    }
};

// dynamic binding (passed by reference)
void draw_shape_dynamic(const Drawable& drawable) {
    drawable.draw();
}

// static binding (passed by value)
void draw_shape_static(const Drawable drawable) {
    drawable.draw();
}

int main() {
    std::vector<std::unique_ptr<Drawable>> shapes;  // static type is Drawable

    shapes.push_back(std::make_unique<Circle>());   // dynamic type is Circle
    shapes.push_back(std::make_unique<Triangle>()); // dynamic type is Triangle

    for (const auto& shape : shapes) {
        shape->draw();
        shape->draw(3);                             // unimplemened from Drawable used in case of Triangle
    }

    std::cout << std::endl;
    draw_shape_dynamic(Circle());                   // binding Circle (dynamic)
    
    std::cout << std::endl;
    draw_shape_static(Circle());                    // binding Drawable (static)

    std::cout << std::endl;
    return 0;
}
