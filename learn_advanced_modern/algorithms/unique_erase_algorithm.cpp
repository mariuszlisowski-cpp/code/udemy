#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template <class T>
void print_vector(std::vector<T> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> nums{9, 1, 2, 3, 4, 9};

    // remove duplicate adjacent elements
    auto first_after = std::unique(nums.begin(), nums.end());   // to no avail (nothing adjacent)
    print_vector(nums);

    std::sort(nums.begin(), nums.end());                        // adjacent now (after sorting)
    print_vector(nums);

    first_after = std::unique(nums.begin(), nums.end());        // dupicates removed to the end
    print_vector(nums);

    nums.erase(first_after, nums.end());                        // removed definitely
    print_vector(nums);

    return 0;
}
