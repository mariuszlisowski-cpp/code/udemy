#include "leak/test.hpp"

#include <iostream>
#include <memory>

std::shared_ptr<Test> create() {
    std::cout << std::endl;
    return std::make_shared<Test>();                        // ptr created here
}

void make_talk_ref(std::shared_ptr<Test>& ptr) {            // risk of modifying content
    ptr.reset();                                            // here: decreasing a count
}

void make_talk_val(std::shared_ptr<Test> ptr) {             // creating a local copy
    ptr->talk();
    std::cout << "> count in: " << ptr.use_count() << '\n'; // thus a count increases
}                                                           // count decreases out of scope

int main() {
    /* ways of creating */
    {
    auto j{ std::make_shared<Test>() };                     // c++14
    auto k{ std::shared_ptr<Test>(new Test()) };            // using a constructor of a class
    std::cout << "> pointing to: " << k.get() << '\n';
    k.reset(new Test());                                    // reuse existing (delete previous object if no refs left)
    std::cout << "> pointing to: " << k.get() << "\n";
    }                                                       // deleting both remaining
    
    /* using */
    auto first{ create() };
    auto second{ first};
    std::cout << "> count: " << second.use_count() << '\n';
    make_talk_val(first);                                   // copy a pointer
    make_talk_ref(first);                                   // decreases the count as passing by a reference
    std::cout << "> count: " << second.use_count() << '\n';

    std::cout << "> exitting" << std::endl;

    return 0;
}
