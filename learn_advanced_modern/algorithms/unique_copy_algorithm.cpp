#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template <class T>
void print_vector(std::vector<T> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> nums{1, 1, 2, 3, 4, 4};
    std::vector<int> out;

    // remove duplicate adjacent elements
    auto predicate = [](int n){ return n > 4; };
    std::unique_copy(nums.begin(), nums.end(), std::back_inserter(out));
    print_vector(out);

    return 0;
}
