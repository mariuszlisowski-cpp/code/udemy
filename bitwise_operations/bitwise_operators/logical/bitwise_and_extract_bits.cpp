/* extract bits from another value - symbol & */

#include <cstdint>
#include <iostream>

/* index    7  6  5  4 3 2 1 0th
   power  2^7                2^0
   weight 128 64 32 16 8 4 2 1
  
   truth table:
   0 AND 0 -> 0
   0 AND 1 -> 0
   1 AND 0 -> 0
   1 AND 1 -> 1      both true -> 1
*/

bool isBitOn(uint8_t value) {
    return value != 0 ? true : false;
}

int main() {
    /*          0110 1101 1011 0101
       hex val     6    d    b    5

       mask     0000 0000 0011 1111
       hex                   3    f

       extract              11 0101     <- task objective (6 least significant bits)
       hex                   3    5
     */
    
    auto value{0x6db5};
    auto mask{0x3f};
    auto extracted = value & mask;

    std::cout << std::hex << extracted << std::endl;

    return 0;
}
