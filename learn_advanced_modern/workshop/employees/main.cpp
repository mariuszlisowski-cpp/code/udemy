/*  The program will read in employee records. It will process them
    and display them in order of job title. If several employees have
    the same job title, they should be displayed in order of years
    employed, with the longest-serving employee first.
 */
#include <iomanip>
#include <iostream>
#include <set>

#include "employee.hpp"

void display_set(const std::multiset<Employee>& employees) {
    std::cout << std::setw(20) << std::left << "JOB TITLE (sorted)" 
                << std::setw(10) << std::left << "NAME"
                << "SENIORITY (sorted)" << std::endl;
    for (auto& employee : employees) {
        std::cout << std::setw(20) << std::left << employee.getTitle()
                  << std::setw(10) << std::left << employee.getName() 
                  <<  "works for " << employee.getSeniority() << " years" << std::endl;
    }
}

int main() {
    std::multiset<Employee> employees{
        Employee("Ada", "C++ Developer", 5),
        Employee("Eve", "Python Developer", 3),
        Employee("Kim", "Java Developer", 4),
        Employee("Rob", "C++ Developer", 7),
        Employee("Tom", "C++ Developer", 2),
        Employee("Ros", "Java Developer", 1)
    };

    display_set(employees);

    return 0;
}
