#include <iostream>

class Refrigerator {
public:
    Refrigerator(double temp, bool door = true, bool power = false)
        : temperature_(temp), door_open_(door), power_on_(power) {}

    // streams cannot be passed by value as they are not copyable
    std::ostream& print(std::ostream& os) const {
        os << "> temperatue: " << temperature_  << std::endl
           << "> door is " << (door_open_ ? "open" : "closed") << std::endl
           << "> powered: " << (power_on_ ? "yes" : "no") << std::endl << std::endl;

        return os;
    }

    void close_door() { door_open_ = false; }
    void open_door() { door_open_ = true; }
    void switch_on() { power_on_ = true; }
    void switch_off() { power_on_ = false; }
    
    void set_temperature(double temperature) {
        temperature_ = temperature;
    }

private:
    double temperature_;
    bool door_open_;
    bool power_on_;
};

int main() {
    Refrigerator refrigerator(20);
    refrigerator.print(std::cout);

    refrigerator.switch_on();
    refrigerator.close_door();
    refrigerator.set_temperature(0.5);
    refrigerator.print(std::cout);

    return 0;
}
