#include <iostream>
#include <memory>
#include <string>

class Student {
public:
    Student(const std::string& name) : name_(name) {
        std::cout << "# creating " << name_ << std::endl;
    }
    ~Student() {
        std::cout << "# destructing " << name_ << std::endl;
    }

    void setBestFriend(std::shared_ptr<Student> bestFriend) {
        bestFriend_ = bestFriend;
    }

    std::string nameOfBestFriend() const {
        auto bestFriend{ bestFriend_.lock() };                      // return a shared_ptr or empty
        if (bestFriend) {
            return bestFriend->name_;
        }

        return "No best friend yet!";
    }

private:
    std::string name_;
    std::weak_ptr<Student> bestFriend_;                             // SOLVED: not owning an object
                                                                    // thus NO ownership increment
};

int main() {
    auto alice{ std::make_shared<Student>("Alice") };               // create a reference to an object
    auto robin{ std::make_shared<Student>("Robin") };               // saa

    /* cicrular dependency works */
    alice->setBestFriend(robin);
    std::cout << "> refs alice: " << alice.use_count() << '\n';
    robin->setBestFriend(alice);                                    // still one refrence only
    std::cout << "> refs robin: " << robin.use_count() << '\n';

    std::cout << "> alice's: " << alice->nameOfBestFriend() << '\n';
    std::cout << "> robin's: " << robin->nameOfBestFriend() << '\n';

    return 0;
}
