#include <iostream>
#include <stack>

template <typename T>
void pop_stack(std::stack<T>& st) {
    while (!st.empty()) {
        std::cout << st.top() << ' ';
        st.pop();
    }
    std::cout << std::endl;
}

int main() {
    // LIFO (Last In First Out) container adapter
    std::stack<int> st({1, 2, 3});

    st.push(4);
    std::cout << "> top: " << st.top() << std::endl;

    st.pop();
    std::cout << "> top: " << st.top() << std::endl;
    std::cout << "> size: " << st.size() << std::endl;

    pop_stack(st);

    if (st.empty()) {
        std::cout << "> stack is empty now" << std::endl;
    }

    return 0;
}
