#include <iostream>
#include <string>

class Student {
public:
    Student(const std::string& name, size_t id)
        : name_(name)
        , id_(id) {}

    // if a < b is true, then a == b is false and a != b is true
    bool operator<(const Student& other) const {    // method must be const
        return this->name_ < other.name_;           // sorted by name
        // return this->id_ == other.id_);          // may be sorted by id instead
    }

    // if a == b is true, then a < b is false and b < a is false
    bool operator==(const Student& other) const {   // method must be const
        return (this->name_ == other.name_ &&
                this->id_ == other.id_);
    }

    bool operator!=(const Student& other) {
        return !(*this == other);   // reuse of already defined operator==
    }


private:
    std::string name_;
    size_t id_;
};

int main() {
    Student studentA{"Ann", 3345};
    Student studentB{"Eli", 3345};

    std::cout << std::boolalpha << (studentA < studentB) << std::endl;
    std::cout << std::boolalpha << (studentB < studentA) << std::endl;
    std::cout << std::boolalpha << (studentA == studentB) << std::endl;
    std::cout << std::boolalpha << (studentA != studentB) << std::endl;

    return 0;
}
