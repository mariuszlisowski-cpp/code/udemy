#include "calculations_lib/include/addition.hpp" // no need for relative path as target_include_directories exists
#include "calculations_lib/include/division.hpp" // in CMakeLists.txt of library directories
#include "printing_lib/include/print_result.hpp" // but needed for an IDE environment

int main() {
    float first_no = 10;
    float second_no = 5;
    float result_add, result_div;

    result_add = addition(first_no, second_no);
    result_div = division(first_no, second_no);

    print_result("addition", result_add);
    print_result("division", result_div);

    return 0;
}
