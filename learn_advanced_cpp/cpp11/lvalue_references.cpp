#include <iostream>
#include <vector>

class Test {
public:
    Test() {
        pBuffer_ = new int[SIZE]{};
    }
    Test(const Test& other) {                                       // copy c'tor
        memcpy(pBuffer_, other.pBuffer_,
               sizeof(*pBuffer_) * SIZE);
    }
    ~Test() {
        delete[] pBuffer_;
    }

private:
    static const int SIZE{ 100 };
    int* pBuffer_;
};

Test getTest() {
    return Test();                                                  // returning by value (temporary copy)
}

int main() {
    Test testA;                                                      // lvalue (has an address)

    Test& rTestA = testA;                                            // bind lvalue only allowed

    // Test& rTestB = getTest();                                    // error: lvalue ref. = rvalue (Test)
    const Test& rTestB = getTest();                                 // ok: extended lifetime of a reference
    Test testB(Test());                                             // copy c'tor accepts const reference

    return 0;
}
