#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

int main() {
    std::ostringstream oss;
    std::string str;

    oss << std::setw(9) << std::setfill('.') << "test";

    std::cout << oss.str() << std::endl;

    return 0;
}
