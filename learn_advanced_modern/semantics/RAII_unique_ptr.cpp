#include <iostream>
#include <memory>

std::unique_ptr<int> function() {
    return std::make_unique<int>(7);                        // calls move constructor (no need to use move function)
}

// overloaded (passing an argument by value)
void function(std::unique_ptr<int> u_ptr) {}

int main() {
    std::unique_ptr<int> uptrA = std::make_unique<int>(9);

    /* ERROR: copy constructor is implicitly deleted
    function(uptrA); // SHOULD USE MOVE CONSTRUCTOR ? */
    
    function(std::move(uptrA));                             // no copy constructor thus move function used
                                                            // uptrA expired (do not use unless assigned again)

    std::unique_ptr<int> uptrB{function()};                 // calls move constructor
    uptrA = function();                                     // calls move assignment

    return 0;
}
