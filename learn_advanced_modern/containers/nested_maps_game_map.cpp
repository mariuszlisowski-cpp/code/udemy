#include <iostream>
#include <map>
#include <string>

using level_map = std::map<int, std::string>;

void print_game_map(const std::map<std::string, level_map>& game_map) {
    for (auto& level : game_map) {
        std::cout << level.first << " : "; 
        for (auto& item : level.second) {
            std::cout << "[" << item.first << " : " << item.second << "] ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

int main() {
    // create levels
    level_map first_level_map { 
        {62, "bullet"},
        {34, "ball"}, 
        {23, "glasses"} 
    };

    level_map second_level_map {
        {78, "boots"},
        {65, "gloves"},
        {32, "shirt"}
    };

    // create game
    std::map<std::string, level_map> game_map {
        {"1st", first_level_map},
        {"2nd", second_level_map}
    };
    print_game_map(game_map);

    // add level to game
    level_map third_level_map;
    third_level_map.insert({34, "ring"});
    third_level_map.insert({44, "throne"});
    third_level_map.insert({98, "powder"});

    game_map.insert({"3rd", third_level_map});
    print_game_map(game_map);
    
    // add level entry
    auto level = game_map.find("1st"); // which level
    if (level != game_map.end()) {
        level->second.insert({65, "gun"}); // new entry
    }

    // remove level entry
    level = game_map.find("3rd");
    if (level != game_map.end()) {
        level->second.erase(98); // 'powder'
    }

    print_game_map(game_map);

    return 0;
}
