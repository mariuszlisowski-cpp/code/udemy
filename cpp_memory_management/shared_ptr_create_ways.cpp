/* ways to create shared pointer */
#include <memory>

struct Foo {
    Foo(int length, double radius)
        : length_(length), radius_(radius) {}

    int length_;
    double radius_;
};

struct Bar {};

int main() {
    /* 1 */
    std::shared_ptr<Foo> s_ptr{ std::make_shared<Foo>(7, 2.89) };       // pass arguments to a type's constructor

    /* 2 */
    Foo* raw_ptr{ new Foo(1, 3 / .14) };
    std::shared_ptr<Foo> from_raw(raw_ptr);                             // takes ownership of a raw pointer
                                                                        // will delete this pointer later

    /* 3 */
    s_ptr.reset(new Foo(9, 7.9));                                       // reusing existing shared pointer
                                                                        // previous object will be deleted
                                                                        // takes ownership of a raw pointer
    return 0;
}
