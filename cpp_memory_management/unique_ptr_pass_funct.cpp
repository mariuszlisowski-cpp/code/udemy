#include <iostream>
#include <memory>

struct Foo {
    double value{ 3.14 };
};

void lending_funct(const std::unique_ptr<Foo>& foo) {       // unique ptr will NOT be deletes as is const
    std::cout << foo->value << std::endl;
}

void ownership_taking_funct(std::unique_ptr<Foo> foo) {
    foo->value = 7.19;                                       // unique ptr present is this scope
    std::cout << foo->value << std::endl;
}

int main() {
    Foo* foo{ new Foo() };
    std::unique_ptr<Foo> u_ptr(foo);
    
    lending_funct(u_ptr);
    ownership_taking_funct(std::move(u_ptr));               // outer scope is emptied
                                                            // u_ptr not accessible here
    if (!u_ptr) {
        std::cout << "> nullptr!" << std::endl;
    }

    return 0;
}
