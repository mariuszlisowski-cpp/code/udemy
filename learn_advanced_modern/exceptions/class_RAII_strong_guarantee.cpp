#include <iostream>
#include <string>

class BufferManager {
  public:
    BufferManager() {}
    // strong exception guarantee (no instances are created or modified in an error condition)
    BufferManager(int size) : size_(size) {
        buffer_ = new char[size];                   // only 'new' can throw an exception without allocating memory
    }
    // strong exception guarantee
    ~BufferManager() {
        delete [] buffer_; // defined as noexcept
    }

    // strong exception guarantee (copy constructor)
    BufferManager(const BufferManager& other) {
        // no need to check for self-assignment (not possible)
        size_ = other.size_;
        buffer_ = new char[size_];                // only 'new' can throw an exception without allocating memory
        for (size_t i = 0; i < size_; ++i) {
            buffer_[i] = other.buffer_[i];
        }
    }

    // STRONG EXCEPTION GUARANTEE (assignment operator)
    BufferManager& operator=(const BufferManager& rhs) {
        // no need to check for self-assignment (no harm)
        BufferManager temp(rhs);                  // copy constructor can throw an exception
        swap(*this, temp);                        // no swap if exception is thrown thus POINTER is SAFE

        return *this;
    }                                             // temp containing old 'this' is destroyed by destructor (scope end)

    friend void swap(BufferManager& first, BufferManager& second) noexcept;

  private:
    int size_{};
    char* buffer_{nullptr};
};

inline void swap(BufferManager& first, BufferManager& second) noexcept {
    using std::swap;                             // if no overloaded version generic swap is used

    swap(first.size_, second.size_);             // overloaded swap may be used for appropriate custom types
    swap(first.buffer_, second.buffer_);         // swapping each data member
}

int main() {
    /* error: no way to assign a newly created object to itself in a way that calls to copy constructor
    BufferManager buffA(buffA); */

    BufferManager buffA{5};
    BufferManager buffB{buffA};
    
    BufferManager buffC;
    buffC = buffB;


    return 0;
}
