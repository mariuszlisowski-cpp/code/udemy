#include <iostream>
#include <map>
#include <string>

int main() {
    std::map<std::string, int> ages;

    ages["Mike"] = 40;                                              // non-existing key (creating & initializing)
    ages["Elen"] = 35;                                              // as above
    ages["Mary"] = 33;                                              // as above

    // direct read access
    std::cout << "Mary:" << ages["Mary"] << std::endl;
    std::cout << "Gary:" << ages["Gary"] << std::endl;              // non-existing key (default zero initialization)

    // direct write
    ages["Mary"] = 44;                                              // existing unique key (thus replacing value)
    
    // insert pair
    std::pair<std::string, int> add_me_to_map("Pete", 43);          // using pair constructor
    ages.insert(add_me_to_map);
    ages.insert(std::make_pair("Rose", 22));                        // temp object

    // iterator loop
    for (std::map<std::string, int>::iterator it = ages.begin();
        it != ages.end(); ++it)
    {
        std::cout << it->first << ':' <<  it->second << ' ';
    }
    std::cout << std::endl;

    // iterator loop with pair
    for (std::map<std::string, int>::iterator it = ages.begin();
         it != ages.end(); ++it) 
    {
        std::pair<std::string, int> age = *it;
        std::cout << age.first << ':' <<  age.second << ' ';
    }
    std::cout << std::endl;

    // range loop
    for (auto& age : ages) {
        std::cout << age.first << ':' << age.second << ' ';
    }
    std::cout << std::endl;

    // finding
    if (auto it = ages.find("Pete"); it != ages.end()) {
        std::cout << it->first << ':' <<  it->second << ' ';
    }

    return 0;
}
