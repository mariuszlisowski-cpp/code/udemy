#include <fstream>
#include <iostream>
#include <string>

int main() {
    std::fstream file;
    std::string filename{"stats"};

    file.open(filename, std::ios::in);
    if (!file.is_open()) {
        std::cout << "Error reading a file: " << filename << std::endl;
    } else {
        while (file) {
            std::string line;
            std::getline(file, line, ':');                                  // delimiter (to stop reading)

            int population;
            file >> population;
            file >> std::ws;                                                // discard newline character (whitespace)
                                                                            // or file.get() before c++11
            if (!file) {
                break;                                                      // if last newline character
            }
            std::cout << line << " -- " << population << std::endl;
        }
        file.close();
    }


    return 0;
}
