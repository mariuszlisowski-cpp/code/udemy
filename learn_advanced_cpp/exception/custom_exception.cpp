#include <exception>
#include <iostream>

class my_exception : public std::exception {
public:
    virtual const char* what() const noexcept {         // throw() untill c++11
        return "custom exception occured";
    }
};

class test {
public:
    void goes_wrong() {
        throw my_exception();
    }
};

int main() {
    test t;

    try {
        t.goes_wrong();
    } catch (const my_exception& e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}
