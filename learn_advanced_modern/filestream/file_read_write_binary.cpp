#include <fstream>
#include <iostream>

void fill_buffer(char *buffer, size_t buffer_size, char ch) {
    for (size_t i = 0; i < buffer_size; ++i) {
        buffer[i] = ch;
    }
}

void print_buffer(char *buffer, size_t buffer_size) {
    for (size_t i = 0; i < buffer_size; ++i) {
        std::cout << buffer[i];
    }
    std::cout << std::endl;
}

int main() {
    const size_t BUFFER_SIZE = 10;

    std::fstream file; // file must exists as is opened for reading
    file.open("example",  std::ios::in | std::ios::out |std::ios::binary); // input & output modes
    // file.open("example",  std::ios::in | std::ios::out); // input & output modes
    if (!file.is_open()) {
        std::cout << "File open error!" << std::endl;
        exit(0);
    } 

    // fill buffer
    char buffer[BUFFER_SIZE];
    fill_buffer(buffer, BUFFER_SIZE, '\x21'); // original value
    print_buffer(buffer, BUFFER_SIZE);

    // persit buffer in a file 
    file.write(buffer, BUFFER_SIZE);
    file.flush();

    // override buffer
    fill_buffer(buffer, BUFFER_SIZE, '\x22'); // other value
    print_buffer(buffer, BUFFER_SIZE);

    file.seekg(0); // set a get pointer to the begining

    // restore buffer from a file
    if (!file.read(buffer, BUFFER_SIZE)) {
        std::cout << "File read error!" << std::endl;
    } else {
        file.flush();
    }

    print_buffer(buffer, BUFFER_SIZE);

    file.close();

    return 0;
}
