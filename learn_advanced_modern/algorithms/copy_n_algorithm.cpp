#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print_vector(const std::vector<T> vec) {
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(std::cout, " "));
}

int main() {
    const size_t size = 2;
    std::vector<int> in {-1, 2, 3, 4, 5};
    std::vector<int> out(size); // sufficient size must be set

    std::copy_n(in.begin(), size, out.begin()); // danger: 'out' must have sufficient size
    print_vector(out);

    std::vector<int> out_safe;
    std::copy_n(in.begin(), in.size(), std::back_inserter(out_safe)); // safe
    print_vector(out_safe);

    return 0;
}
