#include <bitset>
#include <iostream>

int main() {
    int decimal = 8449;                                                 // signed
    std::cout << decimal << "\t0b";
    std::bitset<16> binary(decimal);                                  // max for 16 bits: 65'535 (2^16)
    std::cout << binary << std::endl;

    int ones_complement = ~decimal;
    std::cout << ones_complement <<  "\t0b";
    std::bitset<16> ones_complement_binary(ones_complement);
    std::cout << ones_complement_binary << std::endl;

    return 0;
}
