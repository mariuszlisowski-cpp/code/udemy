#include <algorithm>
#include <iostream>
#include <vector>

class is_longer_than {
public:
    is_longer_than(size_t n) : n_(n) {}
    bool operator()(const std::string& str) const {
        return str.size() > n_; 
    }
private:
    size_t n_;
};

int main() {
    std::vector<std::string> words{"a", "collection" , "of", "words", "with", "varying", "lengths"};
    
    // length limit
    size_t WORD_LENGTH = 5;
    // constexpr size_t WORD_LENGTH = 5; // no need to capture in lambda if constexpr

    // class version
    auto it_class = std::find_if(words.begin(), words.end(), is_longer_than(WORD_LENGTH));
    
    // lambda version
    auto it_lambda = std::find_if(words.begin(), words.end(),
                                 [WORD_LENGTH](const std::string& str) { // returns bool and is const
                                     return str.size() > WORD_LENGTH; // class created with a captured private member
                                 });

    std::cout << "> first found: " << *it_lambda << std::endl;

    return 0;
}
