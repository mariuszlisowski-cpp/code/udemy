#include <chrono>
#include <future>
#include <iostream>
#include <thread>

using namespace std::literals;

int func() {
	std::cout << "> executing function in thread " 
              << std::this_thread::get_id() << std::endl;
	std::this_thread::sleep_for(2s);
	return 42;
}

int main() {
	std::cout << "> running main in thread " 
              << std::this_thread::get_id() << std::endl;

	std::cout << "# calling function" << std::endl;
    auto result = std::async(std::launch::deferred, func);          // run on this thread
	
    std::this_thread::sleep_for(2s);
	
    std::cout << "# calling get(): ";
    std::cout << result.get() << std::endl;
}
