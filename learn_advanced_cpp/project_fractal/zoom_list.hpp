#pragma once

#include <vector>
#include <utility>

#include "zoom.hpp"

namespace raisin {

class zoom_list {
public:
    zoom_list(int width, int height);

    void add(const zoom& z);
    std::pair<double, double> do_zoom(int x, int y);

private:
    int width_{};
    int height_{};
    double x_center_{};
    double y_center_{};
    double scale_{1.0};
    std::vector<zoom> zooms_;
};

}
