#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

// predicate
class is_longer {
public:
    bool operator()(const std::string& lhs, const std::string& rhs) {
        return rhs.size() < lhs.size();
    }
};

template<class T>
void print_container(const T& container) {
    std::for_each(container.begin(), container.end(),
                  [](auto& el) -> void{
                      std::cout << el << std::endl;
                  }); 
}

void sort_longer_first(std::vector<std::string> strings) {
    // algorithm with predicate (class functor here)
    std::sort(strings.begin(), strings.end(), is_longer()); // call class as a functor
    print_container(strings);
}

int main() {
    std::vector<std::string> words{"a", "collection" , "of", "words", "with", "varying", "lengths"};

    sort_longer_first(words);

    return 0;
}
