#include <atomic>
#include <iostream>
#include <string>
#include <thread>

std::atomic_flag lock = ATOMIC_FLAG_INIT;                       // must be initialized (to false)

void print(std::string str) {
    for (size_t i = 0; i < 5; ++i) {
        while (lock.test_and_set()) {}                          // lock the flag (thread spinning in an empty loop)
                                                                // processor-intesive (should spin briefly)
        std::cout << str[0] << str[1] << str[2] << std::endl;   // critical section
        lock.clear();                                           // unlock the flag
    }
}

int main() {
    std::thread th1(print, "abc");
    std::thread th2(print, "def");
    std::thread th3(print, "ghi");

    th1.join();
    th2.join();
    th3.join();

    return 0;
}
