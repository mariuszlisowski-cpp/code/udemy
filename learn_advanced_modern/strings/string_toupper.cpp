#include <cctype>
#include <iostream>
#include <string>

int main() {
    std::string str("Hello, world!");

    std::cout << str << std::endl;
    // char must be a reference
    for (auto& ch : str) {
        ch = std::toupper(ch);
    }
    std::cout << str << std::endl;


    return 0;
}
