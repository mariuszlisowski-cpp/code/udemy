#include <algorithm>
#include <cstddef>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <set>
#include <string>
#include <vector>

template<typename T>
void print_vector(std::vector<T> vec) {
    for (auto el : vec) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    constexpr size_t size = 10;
    std::vector<int> vec;

    std::random_device rd;
    static std::mt19937 mt(rd());
    std::uniform_int_distribution<int> distr(0, 1000);

    // fill a vector with 5 random integers between 0 and 1000
    std::generate_n(std::back_inserter(vec), size, [&](){ return distr(mt); });

    // display the vector elements on the screen
    print_vector(vec);


    // find the maximum element in the vector
    auto it_max = std::max_element(vec.begin(), vec.end());
    std::cout << "> max: " << *it_max << std::endl;

    // find the index of this element
    int index = it_max - vec.begin();    
    std::cout << "> index of max: " << index << std::endl;

    // multi the elements of the vector
    int multi = std::accumulate(vec.begin(), vec.end(), 0);
    std::cout << "> multi of all: " << multi << std::endl;

    // Normalise the vector and put the results into a vector of doubles
    std::vector<double> output;
    std::transform(vec.begin(), vec.end(),
                   std::back_inserter(output),
                   [&it_max](int el){
                       return 1.0 * el / *it_max; // divide all the elements by the largest
                   });
    // print_vector(output);

    // count the number of odd numbers in the original vector
    auto odd_numbers_count = std::count_if(vec.begin(), vec.end(), [](int el){ return el % 2;});
    std::cout << "> odd numbers count: "  << odd_numbers_count << std::endl;

    // make a sorted copy of the vector
    std::vector<int> vec_sorted(vec);
    std::sort(vec_sorted.begin(), vec_sorted.end());
    print_vector(vec_sorted);

    // find the first element greater than...
    constexpr size_t to_be_found = 455;
    auto it_greater_than = std::upper_bound(vec_sorted.begin(), vec_sorted.end(), to_be_found);

    // find the number of elements > 455
    std::cout << "> greater than " << to_be_found << ": " << vec_sorted.end() - it_greater_than << std::endl; // or...
    size_t count = std::count_if(vec_sorted.begin(), vec_sorted.end(), [](int el){ return el > to_be_found;});
    std::cout << "> greater than " << to_be_found << ": " << count << std::endl;

    // copy all the odd numbers to a vector of doubles
    std::vector<double> vec_odd;
    std::copy_if(vec_sorted.begin(), vec_sorted.end(), std::back_inserter(vec_odd),
              [](int el) { return el % 2; });
    print_vector(vec_odd);

    // sort the vector in descending order
    std::sort(vec_odd.begin(), vec_odd.end(), std::greater<>());
    print_vector(vec_odd);

    // randomly shuffle all but the first and the last element of the vector
    std::shuffle(vec_odd.begin() + 1, vec_odd.end() - 1, mt);
    print_vector(vec_odd);

    // remove all the odd numbers from the vector    
    auto it_removed = std::remove_if(vec_sorted.begin(), vec_sorted.end(), [](int el) { return el % 2; });
    if (it_removed != vec_sorted.end()) {
        vec_sorted.erase(it_removed, vec_sorted.end());
    }
    print_vector(vec_sorted);

    // write the these elements to a text file on a single line as a comma separated list, without a trailing comma
    std::ofstream file_out("test");
    if (file_out && vec_sorted.begin() != vec_sorted.end()) {
        std::copy(vec_sorted.begin(), vec_sorted.end() - 1,
                    std::ostream_iterator<int>(file_out, ","));
        file_out << *(vec_sorted.end() - 1); // last element without comma
        std::cout << "> written to file!" << std::endl;
        file_out.close();
    }

    // calculate the factorial of...
    constexpr size_t factorial = 6;
    std::vector<int> nums(factorial);
    std::iota(nums.begin(), nums.end(), 1);
    unsigned long result = std::accumulate(nums.begin(), nums.end(), 1,
                                           [](unsigned long multi, int el){
                                               return multi * el;
                                           });
    std::cout << "> factorial of " << factorial << ": " << result << std::endl;

    // Read a word from input and display all the possible permutations of it
    size_t counter{};
    std::string word_permut{"abac"};
    std::sort(word_permut.begin(), word_permut.end()); // all permutations if sorted in ascending order
    do {
        std::cout << word_permut << ' ';
        ++counter;
    } while (std::next_permutation(word_permut.begin(), word_permut.end()));
    std::cout << "\n> number of permutations: " << counter << std::endl;

    // 

    return 0;
}
