#ifndef PRINT_RESULT_HPP
#define PRINT_RESULT_HPP

#include <string>

void print_result(std::string result_type, float result_value);

#endif
