#include <functional>
#include <iostream>

using namespace std::placeholders;                          // included in functional

int add(int a, int b, int c) {
    std::cout << a << '+' << b << '+' << c << '=';

    return a + b + c;
}

int run(std::function<int(int, int)> func) {                // accepts two ints and returns int
    return func(7, 3);
}

class Test {
public:
    int add(int a, int b, int c) {
        std::cout << a << '+' << b << '+' << c << '=';

        return a + b + c;
    }

};

int main() {
    std::cout << add(1, 2, 3) << std::endl;

    auto calc{ std::bind(add, 4, 5, 6) };                   // variable bind to a function
    std::cout << calc() << std::endl;                       // using a binded function

    auto sumA{ std::bind(add, _1, _2, _3) };                // binding with placeholders
    std::cout << sumA(7, 8, 9) << std::endl;

    auto sumB = std::bind(add, _3, _2, _1);                 // reverse order of placeholders
    std::cout << sumB(7, 8, 9) << std::endl;

    auto sumC = std::bind(add, _1, _2, 99);                 // two placeholders only
    std::cout << sumC(7, 8) << std::endl;

    // using within a function
    std::cout << run(sumC) << std::endl;                    // expect two arguments

    // using within a class
    Test test;
    auto binA{ std::bind(&Test::add, test, -1, -2, -3) };   // address operator copmulsory & object
                                                            // as object can use class data

    return 0;
}
