#include <chrono>
#include <iostream>
#include <future>

unsigned long long fibonacci(unsigned long long n) {
    if (n <= 1) {
		return 1;
    }
	return fibonacci(n - 1) + fibonacci(n - 2);
}

int main() {
	std::cout << "> calling fibonacci\n";                               // main thread

	auto result = std::async(std::launch::async, fibonacci, 42);		// furure promise (separate thread)

	std::cout << "> calculating result...\n";                           // main thread
	
	std::cout << result.get() << std::endl;			                    // waiting for the result
}
