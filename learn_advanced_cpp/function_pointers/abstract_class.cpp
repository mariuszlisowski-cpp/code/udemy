#include <iostream>

/* abstract class - contains pure virtual method(s) */
class Animal {
public:
    virtual void speak() = 0;                                   // pure virtual method
    virtual void run() = 0;                                     // saa
};

class Dog : public Animal {
public:
    virtual void speak() {
        std::cout << "> dog: woof!" << std::endl;
    }
    void run() {}
};

class Labrador : public Dog {
public:
    void run() {
        std::cout << "> labrador: running" << std::endl;
    }
};

void test(Animal& animal) {
    animal.run();
}

int main() {
    Dog* dog = new Dog();
    dog->speak();
    delete dog;

    Animal* animal;

    animal = new Dog();
    animal->speak();
    delete animal;                                              // deleting an object, not a pointer

    animal = new Labrador();                                    // reusing a pointer (safe)
    animal->run();
    animal->speak();
    test(*animal);                                              // pointer dereference to pass as a reference
    delete animal;

    return 0;
}
