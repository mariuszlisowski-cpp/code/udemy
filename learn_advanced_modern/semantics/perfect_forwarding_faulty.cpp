/*  perfect forwarding
    ~ the following properties of the forwarded arguments are preserved:
      - modifable
      - unmodifable
      - moveable
*/

#include <deque>
#include <iostream>
#include <iterator>
#include <type_traits>

class X {};

// accepts modifable, unmodifable or moveable
template <typename T>
void g(T&& arg) {}

// accepts modifable, unmodifable or moveable
template <typename T>
void f(T&& arg) { 
    g(arg);             // if rvalue passed to f(int&&), g receives int& (lvalue but should receive rvalue) ERROR
                        // if lvalue passed to f(int&), g receives int& (lvalue)                            OK

    /* works for rvalue but not for lvalue as will be cast to rvalue
    g(std::move(arg)) // always calls g(int&&) */
}

int main() { 
    X x;
    f(x);               // f(int&) calls g(int&)

    const X xx;
    f(xx);              // f(const int&) calls g(const int&)

    f(X());             // f(int&&) calls g(int&) as inside f function arg will be lvalue
    
    return 0;
}
