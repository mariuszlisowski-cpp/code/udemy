#include <iostream>

// argument as a function pointer
void test(void (*function)() ) {
    function();                                                         // function call
}


int main() {
    [] { std::cout << "I'm an anonymous function"; };
    [] { std::cout << "I'm an anonymous function"; }();                 // call
        
    auto function_ptr{ [] { std::cout << "\nI'm a function_ptr"; } };
    function_ptr();                                                     // call

    test(function_ptr);                                                 // pointer passed
    test([] { std::cout << "\nI'm a function_ptr"; });                  // saa





    return 0;
}
