#include <exception>
#include <iostream>
#include <stdexcept>
#include <string>

class user_input_error : public std::exception {
public:
    explicit user_input_error(const char* message) : message_(message) {} 

    // overridden virtual method from a base class
    const char* what() const noexcept override {
        return message_.c_str();
    }

private:
    std::string message_;
};

void positive_only(int value) {
    if (value < 0) {
        throw user_input_error("ERROR: negative numbers not allowed!");
    }
}

int main() {
    try {
        positive_only(-1);
    } catch (const user_input_error& e) {
        std::cout << e.what() << std::endl;
    }
    
    return 0;
}
