#include <iostream>
#include <thread>

class thread_guard {
  public:
    // compiler generated constructor
    explicit thread_guard() = default;
    // parametrized constructor
    explicit thread_guard(std::thread&& other) noexcept : t(std::move(other)) {}  // thread move constructor called
    // destructor
    ~thread_guard() {
        if (t.joinable()) {
            t.join();
        }
    }

    // no copying
    thread_guard(const thread_guard&) = delete;
    thread_guard& operator=(const thread_guard&) = delete;

    // moving only (compiler generated)
    thread_guard(thread_guard&& other) noexcept = default;
    thread_guard& operator=(thread_guard&& other) noexcept = default;
    
  private: 
    std::thread t;
};

int main() {
    thread_guard guardA;                                                          // default constructor
    guardA = thread_guard();                                                      // move assignment
    thread_guard guardB{std::move(thread_guard())};                               // move constructor

    /* copy constructor deleted
    thread_guard guardC = guardA; */
    
    /* copy assignment deleted
    guardB = guardA; */

    thread_guard guardD((std::thread()));                                         // parametrized constructor

    return 0;
}
