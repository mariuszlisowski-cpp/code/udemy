#include <fstream>
#include <iostream>

void write_old_way() {
    FILE* f = fopen("filename", "w");
    if (f != nullptr) {
        fputs("Hi!", f);
        fclose(f);
    }
}

void write_raii_way() {
    std::ofstream f("filename", std::ios::out);
    f << "Hi!" << std::endl;
}                                                               // object destroyed

int main() {
    

    return 0;
}
