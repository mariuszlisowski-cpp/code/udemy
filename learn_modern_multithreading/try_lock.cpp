#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

using namespace std::literals;

std::mutex the_mutex;

void count() {
    for (size_t i = 0; i < 10; ++i) {
        std::cout << i << ' ';
    }
    std::cout << std::endl;
}

void test() {
    while (!the_mutex.try_lock()) {
        std::this_thread::sleep_for(100ms);
    }
    // critical region executed
    count();
    the_mutex.unlock();
}

int main() {
    std::thread th1(test);
    std::thread th2(test);

    th1.join();
    th2.join();

    return 0;
}
