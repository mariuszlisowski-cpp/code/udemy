#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

struct Language {
    std::string language;
    std::string designer;
    int year;
};

std::vector<Language> read_file(std::string filename) {
    std::vector<Language> languages;
    std::ifstream in_file(filename);
    
    std::string line;
    // C Kernighan & Ritchie 1970		
    while (std::getline(in_file, line)) {
		Language lang;
		std::istringstream iss(line);
		iss >> lang.language;
		std::ostringstream oss;
		bool compound{false};
		while (true) {
			std::string temp;
			iss >> temp;
			try {
				lang.year = std::stoi(temp);
				lang.designer = oss.str();
				break;
			}
			catch (std::invalid_argument) {
				oss << (compound ? " " : "") << temp;
				compound = true;
			}
		}
		languages.push_back(lang);
	}

    return languages;
}

void display_languages(const std::vector<Language>& languages) {
    for (const auto& language : languages) {
        std::cout << "Language: " << language.language << std::endl;
        std::cout << "Designer: " << language.designer << std::endl;
        std::cout << "In year:  " << language.year << std::endl << std::endl;
    }
}

int main() {
    std::string filename{"languages.txt"};
    
    std::vector<Language> languages{read_file(filename)};
    display_languages(languages);

    return 0;
}
