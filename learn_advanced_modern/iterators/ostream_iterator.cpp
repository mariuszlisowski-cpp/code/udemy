#include <iostream>

int main() {
    // writing to a console
    std::ostream_iterator<int> os_it(std::cout, "\n"); // newline delimiter as string literal
    for (size_t i = 0; i < 5; ++i) {
        *os_it = i;     // value to send
        ++os_it;        // iterator increment 
    }

    return 0;
}
