#include "buffer_manager.hpp"

#include <new>
#include <iostream>

int main() {
    try {

        BufferManager bmA(4);
        BufferManager bmB;                  // empty

        bmA.populate_buffer("cstring");
        bmA.print_buffer();

        bmB = bmA;                          // copied (assignment operator)
        bmB.print_buffer();

        BufferManager bmC(bmB);             // copied (copy constructor)
        bmC.print_buffer();
        
    } catch (const std::bad_alloc& e) {
        std::cout << e.what() << std::endl;
    }
    

    return 0;
}
