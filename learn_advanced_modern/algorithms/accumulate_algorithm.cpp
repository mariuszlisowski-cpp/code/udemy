#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

template <class T> void print_container(const T &cont) {
    std::for_each(cont.begin(), cont.end(),
                  [](int val) { std::cout << val << ' '; });
}

template <class T> void print_even_container(const T &cont) {
    std::for_each(cont.begin(), cont.end(), [](int val) {
        if (val % 2 == 0) {
            std::cout << val << ' ';
        }
    });
}

int main() {
    std::vector<int> nums{1, 2, 3, 4, 5, 6};
    print_container(nums);

    // default plus operator used
    int sum = std::accumulate(nums.begin(), nums.end(), 0); // init value
    std::cout << "\n> sum of all values: " << sum << std::endl;

    // custom binary predicate (takes the current sum and the element)
    int sum_from_second =
        std::accumulate(nums.begin(), nums.end(), 0,
                        [](int sum, int value) { return value >= 2 ? sum + value : sum; });
    std::cout << "> sum without first: " << sum_from_second << std::endl;

    print_even_container(nums);
    
    // sum only even numbers
    int sum_even =
        std::accumulate(nums.begin(), nums.end(), 0, [](int sum, int value) {
            return value % 2 ? sum : sum + value;
        });
    std::cout << "\n> sum of even numbers: " << sum_even << std::endl;


    return 0;
}
