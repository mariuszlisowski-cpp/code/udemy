#include <iostream>

class Vehicle {
public:
    void start() {
        std::cout << "> engine started" << std::endl;
    }
};

class Aeroplace : public Vehicle {
public:
    // method overriden
    void start() {
        Vehicle::start(); // parent class method call
        std::cout << "> ready to take off" << std::endl;
    }
};


int main() {
    Aeroplace plane;

    plane.start();              // method overriden

    return 0;
}
