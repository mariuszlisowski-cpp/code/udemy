#include <algorithm>
#include <cctype>
#include <iostream>
#include <string>

int main() {
    std::string str{"only uppercase"}; // not yet

    std::for_each(str.begin(), str.end(), [](char& c) {
        c = std::toupper(c);
    });

    std::cout << str << std::endl;

    return 0;
}
