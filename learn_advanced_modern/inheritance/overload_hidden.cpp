#include <iostream>

class Vehicle {
public:
    void accelerate() {
        std::cout << "> vehicle accelerating" << std::endl;
    }
};

class Aeroplace : public Vehicle {
public:
    // method overloaded (hides inherited method)
    void accelerate(int rate) {
        Vehicle::accelerate();  // NOT a good solution
        std::cout << "> vehicle accelerating with rate " << rate << std::endl;
    }
};


int main() {
    Aeroplace plane;

    plane.accelerate(3);        // method overloaded
    /* inherited method hidden
    plane.accelerate(); */

    return 0;
}
