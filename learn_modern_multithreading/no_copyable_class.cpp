#include <iostream>

class NoCopyable {
public:
    NoCopyable() = default;
    NoCopyable(NoCopyable&&) noexcept = default;
    NoCopyable& operator=(NoCopyable&&) noexcept = default;

    NoCopyable(const NoCopyable&) = delete;
    NoCopyable& operator=(const NoCopyable&) = delete;

};

int main() {
    NoCopyable o1, o2;

    // o2 = o1;                     // no copy assignment
    o2 = std::move(o1);

    // NoCopyable o3(o1);           // no copying
    NoCopyable obj{NoCopyable()};

    return 0;
}
