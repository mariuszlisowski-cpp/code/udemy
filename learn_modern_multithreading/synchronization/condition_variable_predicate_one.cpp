#include <condition_variable>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>

using namespace std::literals;

std::mutex the_mutex;
std::condition_variable cv;
std::string shared_data{"started"};
bool condition{};                                       // flag for condition variable

// waiting thread
void reader() {
    std::unique_lock<std::mutex> ul(the_mutex);
    std::cout << "> read thread waiting...\n";
    cv.wait(ul, []{                                     // wait for condition variable to be notified 
        return condition;                               // predicate chcecking already sent notifications
    });                                                 // avoiding spurious wakeup (only genuine accepted)
    std::cout << "> read thread has "
              << shared_data << std::endl;              // critical section (executed after waking up)
}

// modifying thread
void writer() {
    std::cout << "> writing data..." << std::endl;
    {
        std::lock_guard<std::mutex> lg(the_mutex);
        shared_data = "finished";                       // critical section
        condition = true;                               // avoiding race condition
    }
    cv.notify_one();                                    // notify other thread to wake up
}

int main() {
    std::thread write(writer);                          // condition variable notified
    std::this_thread::sleep_for(500ms);
    std::thread read(reader);                           // lost wake up call avoided by using predicate

    read.join();
    write.join();

    return 0;
}
