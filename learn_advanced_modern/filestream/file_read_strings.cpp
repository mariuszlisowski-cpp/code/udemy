#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

template<typename T>
void printVector(std::vector<T> v) {
    for (const T& e : v) {
        std::cout << e << ' ';
    }
}

int main() {
    std::ifstream ifs;
    ifs.open("numbers");

    std::vector<std::string> numbers;
    if (ifs.is_open()) {
        std::string number;
        while (ifs >> number) {
            numbers.push_back(number);
        }
        ifs.close();
    } else {
        std::cout << "File open error!" << std::endl;
        exit(1);
    }
    printVector(numbers);

    return 0;

}
