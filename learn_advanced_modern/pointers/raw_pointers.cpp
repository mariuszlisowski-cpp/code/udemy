#include <iostream>

int main() {
    int array[]{1, 2, 3};       // actually array is a pointer to the first element
    int* arr;
    arr = array;                // equivalent &array[0]

    size_t size = sizeof(array) / sizeof(*array);
    std::cout << size << std::endl;

    int first = *array;
    int second = *(array + 1);  // array CANNOT be dec/incremented --/++ (only indexed)
    int third = *(array + 2);   // or array[2]
    std::cout << first << ' ' << second << ' ' << third << std::endl;

    int *p = array;
    for (int i = 0; i < size; ++i) {
        std::cout << *p << ' ';
        ++p;                    // pointer can be incremented
    }

    const char c_string[] = "Hi!"; // cstring is null terminated (\0)
    for (const char* c = c_string; *c != '\0'; ++c) {
        std::cout << *c;
    }

    return 0;
}
