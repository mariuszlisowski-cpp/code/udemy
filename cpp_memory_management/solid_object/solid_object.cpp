/*  Solidd objects as members
    Advantages:
    - goes out of scope with the object
    - simpler memory management
    Drawbacks:
    - increases size of owing object
    - cannot be optional
    - no polymorphism
 */
#include "order.hpp"

int main() {
    {
        Order order(Customer("Brian"));
    }                                       // order object destroyed as well as the customer within

    return 0;
}
