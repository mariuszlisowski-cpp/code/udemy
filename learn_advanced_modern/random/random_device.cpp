#include <algorithm>
#include <iostream>
#include <random>

int main() {
    // true random number generator (TRNG)
    // hardware generated random number from system entropy data
    // if supported by by the system and a compiler (otherwise pseudo-random)
    std::random_device rd; // limited performance (much slower then mt19937)

    for (size_t i = 0; i < 5; ++i) {
        std::cout << rd() << ' ';
    }

    return 0;
}
