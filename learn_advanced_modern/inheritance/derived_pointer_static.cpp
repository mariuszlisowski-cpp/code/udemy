#include <iostream>

class Drawable {
public:
    void test() {}
};

class Circle : public Drawable {
public:
    void draw_circle() {}
};

int main() {
    Circle circle;                        // instance contains an instance of the base class (Drawable)
    // static type (given by a compiler)
    Drawable* drawable = &circle;         // binds pointer to base class to Circle instance
    circle.test();
    circle.draw_circle();

    drawable->test();                     // refrences to the basce class part of Circle object only
    /* no reference to Circle
    drawable->draw_circle(); */
    
    return 0;
}
