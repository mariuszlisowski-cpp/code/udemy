#include <bitset>
#include <iostream>

/*  sign bit used to fill the gap:

    positive 0111111111111111    = 32767
    add 0 -> 0011111111111111    = 16383
             ^
    negative 1000000000000000    = -32768
    add 1 -> 1100000000000000    = -16384
             ^ 
      sign bit 
*/

int main() {
    /* fill ZEROes */
    int16_t positive{0b0111111111111111};                              // signed 32767 (max of int16)
    auto result = positive >> 1;                                   // sign bit used to fill from left (0)
        
    std::cout << positive << '\t'
              << std::bitset<16>(positive) << std::endl;
    
    std::cout << result << '\t'
              << std::bitset<16>(result) << std::endl;

    /* fill ONEs */
    int16_t negative{static_cast<int16_t>(0b1000000000000000)};         // signed -32768 (min of int16)
    result = negative >> 1;                                             // sign bit used to fill from left (1)
        
    std::cout << negative << '\t'
              << std::bitset<16>(negative) << std::endl;
    std::cout << result << '\t'
              << std::bitset<16>(result) << std::endl;

}
