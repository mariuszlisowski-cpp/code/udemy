#include <iostream>
#include "test.hpp"

int main(int argc, const char * argv[]) {
    /* using the stack */
    { Test stack; }                                   // end of scope (deleted)
    
    /* using the free store */
    Test* freeStore = new Test();
    freeStore->talk();
    delete freeStore;                                 // deleted by an operator

    /* using the heap */
    Test* heap = (Test*)malloc(sizeof(Test));         // constructor NOT called
    heap->talk();
    free(heap);                                       // destructor NOT called

    return 0;
}
