#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

using namespace std::literals;

class Type {
public:
    void do_it() {
        std::cout << "> pointer used by thread " 
                  << std::this_thread::get_id() << std::endl;
    }
};

Type* type{nullptr};                                            // value to be lazy initialized
std::once_flag type_flag;                                       // flat to initialize call_once

void process() {
    std::call_once(type_flag, []{
        std::cout << "> pointer lazy initialized by thread " 
        << std::this_thread::get_id() << std::endl;
        type = new Type();                                      // once_call instead of double-checked locking
    });                                                         // thread safe since c++11
                                                                
    type->do_it();
}

int main() {
    const unsigned THREADS = 25;
    std::vector<std::thread> threads;
    threads.reserve(THREADS);                                   // count of threads accepted here ONLY

    for (size_t i{0}; i < THREADS; ++i) {
        threads.push_back(std::thread(process));
    }
    
    std::this_thread::sleep_for(500ms);

    for (size_t i{0}; i < THREADS; ++i) {
        threads[i].join();
    }

    return 0;
}
