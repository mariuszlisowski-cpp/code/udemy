#include <ctime>
#include <iostream>
#include <stdexcept>

class Circle {
public:
    Circle(float radius) {
        if (radius < 0) {
            throw std::invalid_argument("Negative radius error!");
        }
    }

private:
};

int main() {
    try {
        Circle c(-3.4);
    } catch (const std::out_of_range& e) {
        std::cerr << e.what() << std::endl;
    } catch (...) {                                             // catch all handler (must be the last or alone)
        std::cerr << "Unhandled exception!" << std::endl;       // useful for testing (to put inside main)
    }
    
    return 0;
}
