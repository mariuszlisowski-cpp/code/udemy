#include <iostream>
#include <string>
#include <typeinfo>

int main() {
    auto name = "Alice";                                    // const char*
    std::cout << typeid(name).name() << std::endl;          // P refers to "pointer", K refers to "const", c is "char"
    
    std::string str;
    decltype(str) surname = "Smith";                        // string
    std::cout << typeid(surname).name() << std::endl;

    return 0;
}
