#include <iostream>
#include <string>

class Complex {
public:
    Complex() {}
    Complex(double r, double i) : real(r), imag(i) {}

    // default copy constructor is enough (as simple data members only)
    // default assignment operator is enouth

    // member method
    Complex operator+=(const Complex& rhs) {
        this->real += rhs.real;
        this->imag += rhs.imag;
        
        return *this; // return by value
    }

    std::string getComplexNo() {
        return std::to_string(real).append(" + ").append(std::to_string(imag)).append("i");
    }

private:
    double real{0.0};
    double imag{0.0};
};

// non-member metod
Complex operator+(const Complex& lhs, const Complex& rhs) {
    Complex temp{lhs};  // default copy constructor used
    temp += rhs;        // member class operator+= method used

    return temp;        // return new instance
}


int main() {
    Complex complA{3, 2};
    Complex complB{2, 1};
    Complex complC;

    complA += complB;   // complA.operator+=(complB)
    std::cout << complA.getComplexNo() << std::endl;

    complC = complA + complB; // default assignment operator used
    std::cout << complC.getComplexNo() << std::endl;
    
    return 0;
}
