#include <iostream>

#include "complex_number_class.hpp"

// both header & source within the same namespace
namespace complex {

Complex::Complex() {}
Complex::Complex(double real, double imaginery)
    : real_(real)
    , imaginary_(imaginery) {}

Complex::Complex(const Complex& other)
    : real_(other.real_), imaginary_(other.imaginary_) {}

Complex& Complex::operator=(const Complex& other) {
    if (this != &other) {                                               // self-assignment check
        real_ = other.real_;
        imaginary_ = other.imaginary_;
    }

    return *this;
}

std::ostream& Complex::print(std::ostream& os) const {                  // must be const as called on const reference
    if (imaginary_ >= 0) {
        os << real_ << " + " << imaginary_ << 'i';
    } else {
        os << real_ << " - " << -imaginary_ << 'i';
    }

    return os;
}

Complex Complex::add_complex(const Complex& rhs) const {
    return Complex(this->real_ + rhs.real_,
                   this->imaginary_ + rhs.imaginary_);
}

Complex Complex::add_real(const double rhs) const {
    return Complex(this->real_ + rhs, this->imaginary_);
}

bool Complex::operator==(const Complex& other) {
    return this->real_ == other.real_ &&
           this->imaginary_ == other.imaginary_;
}

bool Complex::operator!=(const Complex& other) {
    return !(*this == other);                                              // reuse of equality oprator
}

Complex Complex::operator*() const {
    return Complex(real_, -imaginary_);
}

/* out of class definitions below -
   no friendship needed as class's internal method used) */

std::ostream& operator<<(std::ostream& os, const Complex& complex) {
    complex.print(os);                                                  // no need to assign as passed by reference
                                                                        // os changed by a method
                                                                        // complex is a const reference thus print
                                                                        // method must be const
    return os;
}

Complex operator+(const Complex& lhs, const Complex& rhs) {
    return lhs.add_complex(rhs);
}

Complex operator+(const Complex& lhs, const double rhs) {
    return lhs.add_real(rhs);                                           // add only real part of a complex number
}

Complex operator+(const double lhs, const Complex& rhs) {
    return rhs.add_real(lhs);                                           // add only real part of a complex number
}

} // namespace complex
