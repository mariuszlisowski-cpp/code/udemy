#include <iostream>
#include <iterator>
#include <string>

class Point {
public:
    Point() : x_(0), y_(0) {}
    Point(const Point& other) : x_(other.x_), y_(other.x_) {}
    
    // move constructor
    Point(Point&& other) : x_(std::move(other.x_)), y_(std::move(other.y_)) {   // called by Clazz move constructor
        std::cout << "> point move constructor" << std::endl;
    }

private:
    int x_;
    int y_;
};

class Clazz {
public:
    Clazz() : value_(0), point_(Point()) {}
    Clazz(int value, Point point) : value_(value), point_(point) {
        std::cout << "> clazz parametrized constructor" << std::endl;
    }

    // copy constructor
    Clazz(const Clazz& other) : value_(other.value_), point_(other.point_) {
        std::cout << "> clazz copy constructor" << std::endl;
    }    

    /* move constructor
       both other members modified thus an argument cannot be const
       strong exception guarantee as non-throwing (basic if throwing) */
    Clazz(Clazz&& other) noexcept : point_(std::move(other.point_)) {         // move constructor of Point called
                                                                              // cast to rvalue for class members
        std::swap(value_, other.value_);                                      // but swap for build-in types
                                                                              // or regular assign instead
        std::cout << "> clazz move constructor" << std::endl;
    }

    int getValue() {
        return value_;
    }

private:
    int value_;
    Point point_;
};

int main() {
    Clazz clazzA;

    Clazz clazzB = clazzA;              // copy constructor (not assignment)
    Clazz clazzC(std::move(clazzA));    // move constructor
                                        // clazzA non-existent
    return 0;
}
