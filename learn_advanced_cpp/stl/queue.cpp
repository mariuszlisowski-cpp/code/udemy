#include <iostream>
#include <queue>

class Test {
public:
    Test() = default;                                                       // good practice
    Test(const std::string& name) : name_(name) {}
    // ~Test() {
    //     std::cout << "> object destroyed" << std::endl;
    // }
    Test(const Test& other) = default;
    // Test(Test&& other) : name_(std::move(other.name_)) {
    //     std::cout << "> move constructor" << std::endl;
    // }
    // Test& operator=(const Test&) = default;

    void print() const {                                                    // does not modify an object
        std::cout << name_ << std::endl;
    }

    friend std::ostream& operator<<(std::ostream&, const Test&);

private:
    std::string name_;
};

std::ostream& operator<<(std::ostream& os, const Test& t) {
    os << t.name_ << ' ';

    return os;
}

template<typename T>
void print_queue(std::queue<T> q) {
    std::cout << "# queue: ";
    while (!q.empty()) {
        std::cout << q.front() << ' ';
        q.pop();
    }
    std::cout << std::endl;
}

int main() {
    std::queue<Test> test;                                                  // FIFO data structure
    test.push(Test("aa"));
    test.push(Test("bb"));
    test.push(Test("cc"));
    test.push(Test("dd"));
    print_queue(test);

    Test& top = test.front();                                               // read/write reference
    std::cout << "> front: ";
    top.print();
    
    top = Test("zz");                                                       // replacing top of the queue
    std::cout << "> front changed: ";
    top.print();
    print_queue(test);

    test.pop();                                                             // removing top of the queue
    
    top = test.front();
    std::cout << "> front after popping: ";
    top.print();

    print_queue(test);

    return 0;
}
