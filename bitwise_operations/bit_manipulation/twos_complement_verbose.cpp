#include <bitset>
#include <iostream>

int main() {
    {
    /* one's complement:
        -3  1100
        -2  1101
        -1  1110
        -0  1111    -zero
         0  0000     zero
         1  0001
         2  0010
         3  0011
            ^       sign bit

       two's complement:
        -3  1101
        -2  1110
        -1  1111    as -zero before
         0  0000    zero
         1  0001
         2  0010
         3  0011
            ^       sign bit
    */
    char negative = -3;                                   // two's complement
    std::bitset<4> binary_negative(negative);
    std::cout << (int)(negative) << '\t'
              << binary_negative << std::endl;
    
    char positive = 3;
    std::bitset<4> binary_positive(positive);
    std::cout << (int)(positive) << '\t'
              << binary_positive << std::endl;
    
    }

    return 0;
}
