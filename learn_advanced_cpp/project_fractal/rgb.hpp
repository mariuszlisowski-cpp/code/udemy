#pragma once

namespace raisin {

struct rgb {
    rgb(double red, double green, double blue);
    rgb& operator-(const rgb& other);

    double red_;
    double green_;
    double blue_;
};

}
