#include <iostream>
#include <string>

class Test {
public:
    Test(int id, std::string name) : id_(id), name_(name) {
        ptr_ = new int(99);
    }
    // destructor (rule of three)
    ~Test() {
        delete ptr_;
    }
    // copy constructor (rule of three)
    Test(const Test& other) : id_(other.id_), name_(other.name_) {
        // don't check for self assignment since &other is never 
        // equal to this object (makes no sense)
        ptr_ = new int(*other.ptr_);                                // deep copy (new address acquired
                                                                    // as parametrized c'ctor is not called)        
    }
    /* to adhere to the rule of three a copy assignment operator
       should be implemented here */

    void print_data() const {
        std::cout << id_ << ": " << name_ << std::endl;
    }
    void print_pointer() const {
        std::cout << "pointer value: " << *ptr_ << ", ";            // pointer value (same)
        std::cout << "pointing to: " << ptr_ << ", ";               // address inside the pointer (different)
        std::cout << "pointer address: " << &ptr_ << std::endl;     // address of the pointer (different)
    }

private:
    int id_;
    int* ptr_;
    std::string name_;
};

int main() {
    Test testA(77, "Me");
    Test testB = testA;                                             // custom copy constructor call

    testA.print_data();
    testB.print_data();                                             // data deep copied

    testA.print_pointer();                                          // pointer deep copied (different pointers
    testB.print_pointer();                                          // pointing to the different address - OK)

    // Test testC = testC;                                          // self assignment makes no sense
                                                                    // as object does not exist to be assigned

    return 0;
}
