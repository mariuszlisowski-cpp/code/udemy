#include <algorithm>
#include <iostream>
#include <vector>

using vec_it = std::vector<int>::iterator;

void print_range(const vec_it& lower, const vec_it& upper) {
	std::cout << "> all found numbers: ";
	for (auto it = lower; it != upper; ++it) {
		std::cout << *it << ' ';
	}
	std::cout << std::endl;
}
int main() {
	std::vector<int> nums{1, 2, 2, 2, 3, 4, 5};
	const int values_to_find = 2;

	// range boundaries
	auto it_lower = std::lower_bound(nums.begin(), nums.end(), values_to_find);
	if (it_lower != nums.end()) {
		std::cout << "> first number of lower bound found: " << *it_lower << std::endl;
	}

	auto it_upper = std::upper_bound(nums.begin(), nums.end(), values_to_find);
	if (it_upper != nums.end()) {
		std::cout << "> first number of upper bound found: " << *it_upper << std::endl;
	}
	print_range(it_lower, it_upper);

	// faster alternative (recommended)
	auto pair_it = std::equal_range(nums.begin(), nums.end(), values_to_find);
	print_range(pair_it.first, pair_it.second);

	return 0;
}
