#include <future>
#include <iostream>
#include <thread>

int main() {
	std::packaged_task<int(int, int)> pt([](int a, int b) {
		return a + b;
	});															// non-copyable

	std::future<int> fut{pt.get_future()};                      // the future for packaged_task's promise

	std::thread th(std::move(pt), 3, 2);						// run the thread with packaged_task with params
	th.join();

	std::cout << fut.get() << std::endl;                        // utilization of the future

	return 0;
}
